 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">
                 <h3 class="card-title">
                     <?php if($this->session->userdata('role_id') == 2)  { ?>
                     <a href="<?= base_url('tambahInformasi'); ?>" class="btn btn-danger"><i class="fa fa-plus"></i>
                         Tambah Data</a>
                     <?php } ?>

                     <?php if($this->session->userdata('role_id') == 1)  { ?>
                     <a href="<?= base_url('tambahInformasi'); ?>" class="btn btn-danger"><i class="fa fa-plus"></i>
                         Tambah Data</a>
                     <?php } ?>


                 </h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <?= $this->session->flashdata('message'); ?>
                 <table id="example1" class="table table-bordered table-striped">
                     <thead>
                         <tr>
                             <th>No</th>
                             <th>Judul Tender</th>
                             <th>Ruangan</th>
                             <th>Gedung</th>
                             <th>Area</th>
                             <th>Pagu Anggaran</th>
                             <th>Status</th>
                             <th>#</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php $i = 1; ?>
                         <?php foreach ($v as $info) : ?>
                         <tr>
                             <td><?= $i; ?></td>
                             <td><?= $info['judul_tender']; ?></td>
                             <td><?= $info['nama_ruangan']; ?></td>
                             <td><?= $info['nama_gedung']; ?></td>
                             <td><?= $info['lokasi']; ?></td>
                             <td><?= number_format($info['pagu_anggaran']); ?></td>
                             <td> <?php
							 $today = date('Y-m-d');
							 if ($today >= $info['date_start_tender'] && $today <= $info['date_end_tender']) {
                               echo '<span class="badge badge-success">OPEN</span> ';
                               } else { 
                               echo '<span class="badge badge-danger">Close</span>';}
								?>

                             </td>
                             <td>
                                 <div class="dropdown">
                                     <button type="button" class="btn btn-primary dropdown-toggle"
                                         data-toggle="dropdown">
                                         <i class="fa fa-cog"></i>
                                     </button>
                                     <div class="dropdown-menu">
                                         <a href="<?= base_url('detailInformasi/'); ?><?= $info['reff_tender']; ?>"
                                             class="dropdown-item">Detail Data</a>
                                         <?php if($this->session->userdata('role_id') == 2)  { ?>
                                         <a href="<?= base_url('ubahInformasi/'); ?><?= $info['reff_tender']; ?>"
                                             class="dropdown-item">Ubah data</a>
                                         <a href="<?= base_url('hapusInformasi/'); ?><?= $info['reff_tender']; ?>"
                                             class="dropdown-item tombol-hapus">Hapus Data</a>
                                         <?php } ?>
                                         <?php if($this->session->userdata('role_id') == 1)  { ?>
                                         <a href="<?= base_url('ubahInformasi/'); ?><?= $info['reff_tender']; ?>"
                                             class="dropdown-item">Ubah data</a>
                                         <a href="<?= base_url('hapusInformasi/'); ?><?= $info['reff_tender']; ?>"
                                             class="dropdown-item tombol-hapus">Hapus Data</a>
                                         <?php } ?>
                                         <?php if($this->session->userdata('role_id') == 3)  { ?>
                                         <?php
							             $today = date('Y-m-d');
							             if ($today >= $info['date_start_tender'] && $today <= $info['date_end_tender']) { ?>
                                         <a href="<?= base_url('periksaPenawaran/'); ?><?= $info['reff_tender']; ?>"
                                             class="dropdown-item">Buat Penawaran</a>
                                         <?php } ?>
                                         <?php } ?>
                                     </div>
                                 </div>
                             </td>
                         </tr>
                         <?php $i++; ?>
                         <?php endforeach; ?>
                     </tbody>
                 </table>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>
 </section>
