 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">
                 <h3 class="card-title">
                     Form Informasi Tender
                 </h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <form class="form-horizontal" method="post" action="<?= base_url('tambahInformasi'); ?>"
                     enctype="multipart/form-data">
                     <div class="card-body">
                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Ruangan</label>
                             <div class="col-sm-10">
                                 <select type="text" class="form-control select2bs4" id="ruangan_id" name="ruangan_id"
                                     required>
                                     <option selected disabled value="">Pilih</option>
                                     <?php foreach ($v as $row2) : ?>
                                     <option value="<?= $row2['id_ruangan']; ?>"><?= $row2['nama_ruangan']; ?></option>
                                     <?php endforeach ?>
                                 </select>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Kriteria Pengadaan</label>
                             <div class="col-sm-10">
                                 <select type="text" class="form-control select2bs4" id="swakelola_id"
                                     name="swakelola_id" required>
                                     <option selected disabled value="">Pilih</option>
                                     <?php foreach ($swa as $row3) : ?>
                                     <option value="<?= $row3['id_swakelola']; ?>"><?= $row3['jenis_swakelola']; ?>
                                     </option>
                                     <?php endforeach ?>
                                 </select>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Judul Tender</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="judul_tender" name="judul_tender" required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Keterangan</label>
                             <div class="col-sm-10">
                                 <textarea type="text" rows="5" class="form-control" id="keterangan" name="keterangan"
                                     required></textarea>
                             </div>
                         </div>


                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Pagu Anggaran</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="duit" name="pagu_anggaran" required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Tanggal Mulai Informasi</label>
                             <div class="col-sm-5">
                                 <input type="date" class="form-control" id="date_start_tender" name="date_start_tender"
                                     required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Tanggal Selesai Informasi</label>
                             <div class="col-sm-5">
                                 <input type="date" class="form-control" id="date_end_tender" name="date_end_tender"
                                     required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Target Tanggal Selesai</label>
                             <div class="col-sm-5">
                                 <input type="date" class="form-control" id="date_end_work" name="date_end_work"
                                     required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Lampiran</label>
                             <div class="col-sm-5">
                                 <input type="file" class="form-control custom-file-input" id="lampiran" name="lampiran"
                                     accept="application/pdf, application/vnd.ms-excel" required>
                                 <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                             </div>
                         </div>



                     </div>
                     <!-- /.card-body -->
                     <div class="card-footer">
                         <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                         <a href="#" onclick="history.back();" class="btn btn-default float-right">Kembali</a>
                     </div>
                     <!-- /.card-footer -->
                 </form>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>
 </section>