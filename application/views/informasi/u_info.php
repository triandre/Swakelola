 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">
                 <h3 class="card-title">
                     Form Informasi Tender
                 </h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <form class="form-horizontal" method="post" action="<?= base_url('ubahInformasiGo'); ?>"
                     enctype="multipart/form-data">
                     <div class="card-body">
                         <input hidden type="text" class="form-control" id="reff_tender" name="reff_tender"
                             value="<?= $d['reff_tender']; ?>" required readonly>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Ruangan</label>
                             <div class="col-sm-10">
                                 <select type="text" class="form-control select2bs4" id="ruangan_id" name="ruangan_id"
                                     required>
                                     <?php foreach ($v as $key) { ?>
                                     <option value="<?= $key['id_ruangan']; ?>"
                                         <?= ($d['ruangan_id'] == $key['id_ruangan']) ? 'selected' : ''; ?>>
                                         <?= $key['nama_ruangan']; ?></option>
                                     <?php } ?>
                                 </select>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Kriteria Pengadaan</label>
                             <div class="col-sm-10">
                                 <select type="text" class="form-control select2bs4" id="swakelola_id"
                                     name="swakelola_id" required>
                                     <?php foreach ($swa as $key2) { ?>
                                     <option value="<?= $key2['id_swakelola']; ?>"
                                         <?= ($d['swakelola_id'] == $key2['id_swakelola']) ? 'selected' : ''; ?>>
                                         <?= $key2['jenis_swakelola']; ?></option>
                                     <?php } ?>
                                 </select>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Judul Tender</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="judul_tender" name="judul_tender"
                                     value="<?= $d['judul_tender']; ?>" required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Keterangan</label>
                             <div class="col-sm-10">
                                 <textarea type="text" rows="5" class="form-control" id="keterangan" name="keterangan"
                                     required><?= $d['keterangan']; ?></textarea>
                             </div>
                         </div>


                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Pagu Anggaran</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="duit" name="pagu_anggaran"
                                     value="<?= $d['pagu_anggaran']; ?>" required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Tanggal Mulai Informasi</label>
                             <div class="col-sm-5">
                                 <input type="date" class="form-control" id="date_start_tender" name="date_start_tender"
                                     value="<?= $d['date_start_tender']; ?>" required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Tanggal Selesai Informasi</label>
                             <div class="col-sm-5">
                                 <input type="date" class="form-control" id="date_end_tender" name="date_end_tender"
                                     value="<?= $d['date_end_tender']; ?>" required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Target Tanggal Selesai</label>
                             <div class="col-sm-5">
                                 <input type="date" class="form-control" id="date_end_work" name="date_end_work"
                                     value="<?= $d['date_end_work']; ?>" required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Lampiran</label>
                             <div class="col-sm-5">
                                 <input type="file" class="form-control custom-file-input" id="lampiran" name="lampiran"
                                     accept="application/pdf, application/vnd.ms-excel">
                                 <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                             </div>
                         </div>



                     </div>
                     <!-- /.card-body -->
                     <div class="card-footer">
                         <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Update</button>
                         <a href="#" onclick="history.back();" class="btn btn-default float-right">Kembali</a>
                     </div>
                     <!-- /.card-footer -->
                 </form>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>
 </section>
