 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">
                 <h3 class="card-title">
                     Detail Informasi Tender
                 </h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <form class="form-horizontal" method="post" action="<?= base_url('ubahInformasiGo'); ?>"
                     enctype="multipart/form-data">
                     <div class="card-body">

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-4 col-form-label">Nama Ruangan</label>
                             <div class="col-sm-18">
                                 <label for="inputEmail3"
                                     class="col-sm-18 col-form-label"><?= $d['nama_ruangan']; ?></label>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-4 col-form-label">Kriteria Pengadaan</label>
                             <div class="col-sm-18">
                                 <label for="inputEmail3"
                                     class="col-sm-18 col-form-label"><?= $d['jenis_swakelola']; ?></label>
                                 </select>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-4 col-form-label">Judul Tender</label>
                             <div class="col-sm-18">
                                 <label for="inputEmail3"
                                     class="col-sm-18 col-form-label"><?= $d['judul_tender']; ?></label>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-4 col-form-label">Keterangan</label>
                             <div class="col-sm-18">
                                 <label for="inputEmail3"
                                     class="col-sm-18 col-form-label"><?= $d['keterangan']; ?></label>
                             </div>
                         </div>


                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-4 col-form-label">Pagu Anggaran</label>
                             <div class="col-sm-18">
                                 <label for="inputEmail3"
                                     class="col-sm-18 col-form-label"><?= number_format($d['pagu_anggaran']); ?></label>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-4 col-form-label">Tanggal Mulai Informasi</label>
                             <div class="col-sm-5">
                                 <label for="inputEmail3"
                                     class="col-sm-18 col-form-label"><?= date('d F Y', strtotime($d['date_start_tender'])); ?></label>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-4 col-form-label">Tanggal Selesai Informasi</label>
                             <div class="col-sm-5">
                                 <label for="inputEmail3"
                                     class="col-sm-18 col-form-label"><?= date('d F Y', strtotime($d['date_end_tender'])); ?></label>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-4 col-form-label">Target Tanggal Selesai</label>
                             <div class="col-sm-5">
                                 <label for="inputEmail3"
                                     class="col-sm-18 col-form-label"><?= date('d F Y', strtotime($d['date_end_work'])); ?></label>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-4 col-form-label">Lampiran</label>
                             <div class="col-sm-5">
                                 <label for="inputEmail3" class="col-sm-18 col-form-label"><a
                                         href="<?= base_url('src/archive/').$d['lampiran'] ?>" target="_blank">Lihat
                                         file</a></label>
                             </div>
                         </div>



                     </div>
                     <!-- /.card-body -->
                     <div class="card-footer">
                         <a href="#" onclick="history.back();" class="btn btn-danger"><i class="fa fa-backward"></i>
                             Kembali</a>
                     </div>
                     <!-- /.card-footer -->
                 </form>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>
 </section>