<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="<?= base_url('src/login/'); ?>fonts/icomoon/style.css">

    <link rel="stylesheet" href="<?= base_url('src/login/'); ?>css/owl.carousel.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url('src/login/'); ?>css/bootstrap.min.css">

    <!-- Style -->
    <link rel="stylesheet" href="<?= base_url('src/login/'); ?>css/style.css">

    <title><?= $title; ?></title>
</head>

<body>
    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash_login'); ?>"></div>
    <?php if ($this->session->flashdata('flash_login')) : ?>

    <?php endif; ?>

    <div class="d-lg-flex half">
        <div class="bg order-1 order-md-2" style="background-image: url('src/login/images/bg_1.jpg');"></div>
        <div class="contents order-2 order-md-1">

            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-7">
                        <div class="mb-4">
                            <h3>Sign In</h3>
                            <p class="mb-4">Swakelola Sarana dan Prasarana Universitas Muhammadiyah Sumatera Utara .</p>
                        </div>
                        <?php if ($this->session->flashdata('gagal_login')) { ?>
                        <div class="alert alert-danger">
                            <?= $this->session->flashdata('gagal_login') ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <?php } ?>
                        <form action="<?= base_url('login'); ?>" method="post">
                            <div class="form-group first">
                                <input type="email" class="form-control" placeholder="Email" id="email" name="email"
                                    required>

                            </div>
                            <div class="form-group last mb-3">
                                <input type="password" class="form-control" placeholder="Password" id="password"
                                    name="password" required>

                            </div>

                            <div class="d-flex mb-5 align-items-center">

                                <span class="ml-auto"><a href="#" class="forgot-pass">Forgot Password</a></span>
                            </div>

                            <input type="submit" value="Log In" class="btn btn-block btn-primary">


                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>



    <script src="<?= base_url('src/login/'); ?>js/jquery-3.3.1.min.js"></script>
    <script src="<?= base_url('src/login/'); ?>js/popper.min.js"></script>
    <script src="<?= base_url('src/login/'); ?>js/bootstrap.min.js"></script>

</body>

</html>