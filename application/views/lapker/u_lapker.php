 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">
                 <h3 class="card-title">
                     Form Laporan Pengerjaan
                 </h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <form class="form-horizontal" method="post" action="<?= base_url('u_lapkerGo'); ?>" enctype="multipart/form-data">
                     <div class="card-body">
                         <input hidden type="text" class="form-control" id="reff_lapker" name="reff_lapker" value="<?= $d['reff_lapker']; ?>" required readonly>
                         <input hidden type="text" class="form-control" id="reff_tender" name="reff_tender" value="<?= $reff; ?>" required readonly>
                         <input hidden type="text" class="form-control" id="reff_penawaran" name="reff_penawaran" value="<?= $penawaran; ?>" required readonly>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Keterangan</label>
                             <div class="col-sm-10">
                                 <textarea type="text" rows="10" class="form-control" id="keterangan" name="keterangan" value="<?= $d['ket_progres']; ?>" required><?= $d['ket_progres']; ?></textarea>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Persentase Progress</label>
                             <div class="col-sm-2">
                                 <select type="text" class="form-control select2bs4" id="persen" name="persen" required>
                                     <option value="<?= $d['persen']; ?>"><?= $d['persen']; ?></option>
                                     <?php
                                        for ($i = 5; $i <= 100; $i += 5) {
                                            echo "<option value=\"$i\">$i</option>";
                                        }
                                        ?>
                                 </select>
                             </div>
                             <div class="col-sm-5">
                                 <label class="col-sm-2 col-form-label">%</label>
                             </div>
                         </div>




                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Lampiran</label>
                             <div class="col-sm-2">
                                 <input type="file" class="form-control custom-file-input" id="lampiran_lapker" name="lampiran_lapker" accept="application/pdf, application/vnd.ms-excel">
                                 <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                             </div>
                         </div>



                     </div>
                     <!-- /.card-body -->
                     <div class="card-footer">
                         <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Update</button>
                         <a href="#" onclick="history.back();" class="btn btn-default float-right">Kembali</a>
                     </div>
                     <!-- /.card-footer -->
                 </form>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>
 </section>