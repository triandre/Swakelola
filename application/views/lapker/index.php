 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">
                 <h3 class="card-title">
                     Data Penawaran
                 </h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <?= $this->session->flashdata('message'); ?>
                 <table id="example1" class="table table-bordered table-striped">
                     <thead>
                         <tr>
                             <th>No</th>
                             <th>Pengerjaan</th>
                             <th>Mulai Pengerjaan</th>
                             <th>Target Selesai</th>
                             <th>#</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php $i = 1; ?>
                         <?php foreach ($v as $info) : ?>
                             <tr>
                                 <td><?= $i; ?></td>
                                 <td><?= $info['judul_tender']; ?> <br>
                                 <td> <?= date('d F Y', strtotime($info['date_start_penawaran'])); ?></td>
                                 <td><?= date('d F Y', strtotime($info['date_end_penawaran'])); ?></td>
                                 <td>
                                     <div class="dropdown">
                                         <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                             <i class="fa fa-cog"></i>
                                         </button>
                                         <div class="dropdown-menu">
                                             <a href="<?= base_url('v_lapker/') . $info['reff_tender'] . '/' . $info['reff_penawaran'] ?>" class="dropdown-item">Lihat Progress</a>

                                         </div>
                                     </div>
                                 </td>
                             </tr>
                             <?php $i++; ?>
                         <?php endforeach; ?>
                     </tbody>
                 </table>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>

 </section>