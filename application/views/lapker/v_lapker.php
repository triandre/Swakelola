 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">
                 <h3 class="card-title">
                     <?php if ($this->session->userdata('role_id') == 3) { ?>
                         <a href="<?= base_url('c_lapker/') . $reff . '/' . $penawaran; ?>" class="btn btn-danger"><i class="fa fa-plus"></i>
                             Tambah Data</a>
                     <?php } ?>

                 </h3>
                 <a href="#" onclick="history.back();" class="btn btn-info float-right"><i class="fa fa-backward"></i> Kembali</a>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <?= $this->session->flashdata('message'); ?>
                 <div class="alert alert-success">
                     <strong>Pengerjaan!</strong> <?= $d['judul_tender']; ?>
                 </div>
                 <table id="" class="table ">
                     <thead>
                         <tr>
                             <th>No</th>
                             <th>Tanggal</th>
                             <th>Keterangan</th>
                             <th>Progress</th>
                             <th>Lampiran</th>
                             <th>#</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php $i = 1; ?>
                         <?php foreach ($v as $info) : ?>
                             <tr>
                                 <td><?= $i; ?></td>
                                 <td><?= $info['date_created']; ?></td>
                                 <td><?= $info['ket_progres']; ?></td>
                                 <td>
                                     <div class="progress">
                                         <div class="progress-bar progress-bar-danger" style="width: <?= $info['persen']; ?>%"><span class="badge bg-success"><?= $info['persen']; ?>%</span></div>
                                     </div>
                                 </td>
                                 <td><a href="<?= base_url('src/archive/') . $info['lampiran_lapker']; ?>" target="_blank">Lihat</a> </td>
                                 <td>
                                     <div class="dropdown">
                                         <button type="button" class="btn-sm btn btn-info dropdown-toggle" data-toggle="dropdown">
                                             <i class="fa fa-cog"></i>
                                         </button>
                                         <div class="dropdown-menu">
                                             <a href="<?= base_url('u_lapker/') . $info['reff_lapker'] . '/' . $info['reff_tender'] . '/' . $info['reff_penawaran']; ?>" class="dropdown-item">Ubah Laporan</a>

                                         </div>
                                     </div>
                                 </td>
                             </tr>
                             <?php $i++; ?>
                         <?php endforeach; ?>
                     </tbody>
                 </table>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>

 </section>