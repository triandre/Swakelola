 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">
                 <h3 class="card-title">

                 </h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <?= $this->session->flashdata('message'); ?>
                 <table id="example1" class="table table-bordered table-striped">
                     <thead>
                         <tr>
                             <th>No</th>
                             <th>Judul Tender</th>
                             <th>Ruangan</th>
                             <th>Gedung</th>
                             <th>Area</th>
                             <th>PIC</th>
                             <th>#</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php $i = 1; ?>
                         <?php foreach ($v as $info) : ?>
                         <tr>
                             <td><?= $i; ?></td>
                             <td><?= $info['judul_tender']; ?></td>
                             <td><?= $info['nama_ruangan']; ?></td>
                             <td><?= $info['nama_gedung']; ?></td>
                             <td><?= $info['lokasi']; ?></td>
                             <td> <?= $info['nama_pt']; ?></td>

                             <td>
                                 <div class="dropdown">
                                     <button type="button" class="btn btn-primary dropdown-toggle"
                                         data-toggle="dropdown">
                                         <i class="fa fa-cog"></i>
                                     </button>
                                     <div class="dropdown-menu">
                                         <a href="<?= base_url('detailPemenang/'); ?><?= $info['reff_penawaran']; ?>"
                                             class="dropdown-item">Detail</a>


                                     </div>
                                 </div>
                             </td>
                         </tr>
                         <?php $i++; ?>
                         <?php endforeach; ?>
                     </tbody>
                 </table>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>
 </section>