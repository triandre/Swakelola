 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">
                 <h3 class="card-title">
                     Form Penawaran
                 </h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <form class="form-horizontal" method="post" action="<?= base_url('buatPenawaranGo'); ?>"
                     enctype="multipart/form-data">
                     <div class="card-body">
                         <input hidden type="text" class="form-control" id="reff_tender" name="reff_tender"
                             value="<?= $d['reff_tender']; ?>" required readonly>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Judul Tender</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="judul_tender" name="judul_tender"
                                     value="<?= $d['judul_tender']; ?>" readonly required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Deskripsi</label>
                             <div class="col-sm-10">
                                 <textarea type="text" rows="5" class="form-control" id="deskripsi_penawaran"
                                     name="deskripsi_penawaran" required></textarea>
                             </div>
                         </div>


                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Jumlah Penawaran</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="duit" name="anggaran_penawaran" required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Tanggal Mulai Pengerjaan</label>
                             <div class="col-sm-5">
                                 <input type="date" class="form-control" id="date_start_penawaran"
                                     name="date_start_penawaran" required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Tanggal Selesai Pengerjaan</label>
                             <div class="col-sm-5">
                                 <input type="date" class="form-control" id="date_end_penawaran"
                                     name="date_end_penawaran" required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Nama PIC</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="nama_pic" name="nama_pic" required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">No PIC</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="no_pic" name="no_pic" required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Lampiran Penawaran</label>
                             <div class="col-sm-5">
                                 <input type="file" class="form-control custom-file-input" id="lampiran_penawaran"
                                     name="lampiran_penawaran" accept="application/pdf, application/vnd.ms-excel">
                                 <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                             </div>
                         </div>



                     </div>
                     <!-- /.card-body -->
                     <div class="card-footer">
                         <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Submit</button>
                         <a href="#" onclick="history.back();" class="btn btn-default float-right">Kembali</a>
                     </div>
                     <!-- /.card-footer -->
                 </form>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>
 </section>