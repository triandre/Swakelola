 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">
                 <h3 class="card-title">

                 </h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <?= $this->session->flashdata('message'); ?>
                 <table id="example1" class="table table-bordered table-striped">
                     <thead>
                         <tr>
                             <th>No</th>
                             <th>Judul Tender</th>
                             <th>Ruangan</th>
                             <th>Gedung</th>
                             <th>Area</th>
                             <th>Pagu Anggaran</th>
                             <th>Status</th>
                             <th>#</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php $i = 1; ?>
                         <?php foreach ($v as $info) : ?>
                         <tr>
                             <td><?= $i; ?></td>
                             <td><?= $info['judul_tender']; ?>
                                 <br>
                                 <?php if($info['sts_tender']== 2) { ?>
                                 <span class="badge badge-danger"> Winner</span>
                                 <span class="badge badge-success"> <?= $info['nama_pt']; ?></span>
                                 <?php } ?>
                             </td>
                             <td><?= $info['nama_ruangan']; ?></td>
                             <td><?= $info['nama_gedung']; ?></td>
                             <td><?= $info['lokasi']; ?></td>
                             <td><?= number_format($info['pagu_anggaran']); ?></td>
                             <td> <?php
							 $today = date('Y-m-d');
							 if ($today >= $info['date_start_tender'] && $today <= $info['date_end_tender']) {
                               echo '<span class="badge badge-success">OPEN</span> ';
                               } else { 
                               echo '<span class="badge badge-danger">Close</span>';}
								?>

                             </td>
                             <td>
                                 <div class="dropdown">
                                     <button type="button" class="btn btn-primary dropdown-toggle"
                                         data-toggle="dropdown">
                                         <i class="fa fa-cog"></i>
                                     </button>
                                     <div class="dropdown-menu">

                                         <?php if($this->session->userdata('role_id') == 2)  { ?>
                                         <a href="<?= base_url('lihatPenawaran/'); ?><?= $info['reff_tender']; ?>"
                                             class="dropdown-item">Lihat Penawaran</a>

                                         <?php } ?>
                                         <?php if($this->session->userdata('role_id') == 1)  { ?>
                                         <a href="<?= base_url('lihatPenawaran/'); ?><?= $info['reff_tender']; ?>"
                                             class="dropdown-item">Lihat Penawaran</a>

                                         <?php } ?>
                                     </div>
                                 </div>
                             </td>
                         </tr>
                         <?php $i++; ?>
                         <?php endforeach; ?>
                     </tbody>
                 </table>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>
 </section>