<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    Negosiasi Tender
                </h3>
            </div>
            <div class="card-body">
                <div class="card-body">
                    <div class="row">
                        <div class="col-4">
                            <h3>Tender</h3>
                            <hr>
                            <p>
                                Nama Ruangan: <br> <b><?= $d['nama_ruangan']; ?></b> <br>
                                Kriteria Pengadaan: <br> <b><?= $d['jenis_swakelola']; ?></b> <br>
                                Judul: <br> <b><?= $d['jenis_swakelola']; ?></b> <br>
                                Judul: <br> <b><?= $d['judul_tender']; ?></b> <br>
                                Keterangan: <br> <b><?= $d['keterangan']; ?></b> <br>
                                Pagu Anggaran: <br> <b>Rp. <?= number_format($d['pagu_anggaran']); ?></b> <br>
                                Target Selesai: <br> <b><?= date('d F Y', strtotime($d['date_end_work'])); ?></b>
                                <br>
                                Lampiran: <br> <b><a href="<?= base_url('src/archive/').$d['lampiran'] ?>"
                                        target="_blank">Lihat
                                        file</a></b> <br>
                            </p>

                        </div>

                        <div class="col-4">
                            <h3>Penawaran</h3>
                            <hr>
                            <p> Instansi:
                                <br>
                                <b> <?= $d['nama_pt']; ?></b>
                                <br>
                                Total Penawaran:
                                <br>
                                <b>Rp. <?= number_format($d['anggaran_penawaran']); ?></b>
                                <br>
                                Deskripsi Penawaran:
                                <br>
                                <b><?= $d['deskripsi_penawaran']; ?></b>
                                <br>
                                Nama PIC:
                                <br>
                                <b><?= $d['nama_pic']; ?></b>
                                <br>
                                No. Hp PIC:
                                <br>
                                <b><?= $d['no_pic']; ?></b>
                                <br>
                                Mulai Pengerjaan:
                                <br>
                                <b><?= date('d F Y', strtotime($d['date_start_penawaran'])); ?></b>
                                <br>
                                Selesai Pengerjaan:
                                <br>
                                <b><?= date('d F Y', strtotime($d['date_end_penawaran'])); ?></b>
                                <br>
                                Lampiran Penawaran: <br>
                                <b><a href="<?= base_url('src/archive/').$d['lampiran_penawaran'] ?>"
                                        target="_blank">Lihat
                                        file</a> </b>
                                <br>
                            </p>
                        </div>

                        <div class="col-4">
                            <h3>Persetujuan</h3>
                            <hr>
                            <form method="post" action="<?= base_url('setujuiPenawaranGo'); ?>">
                                <input hidden type="text" class="form-control" id="reff_penawaran" name="reff_penawaran"
                                    value="<?= $d['reff_penawaran']; ?>" required readonly>
                                <input hidden type="text" class="form-control" id="reff_tender" name="reff_tender"
                                    value="<?= $d['reff_tender']; ?>" required readonly>

                                <div class="form-group">
                                    <label>Email Instansi</label>
                                    <input type="text" class="form-control" id="email" name="email"
                                        value="<?= $d['email']; ?>" required readonly>

                                </div>

                                <div class="form-group">
                                    <label>Pembayaran</label>
                                    <select type="text" class="form-control select2bs4" id="pembayaran"
                                        name="pembayaran" required>
                                        <option selected disabled value="">Pilih</option>
                                        <?php foreach ($bayar as $row2) : ?>
                                        <option value="<?= $row2['id_pembayaran']; ?>"><?= $row2['pembayaran']; ?>
                                        </option>
                                        <?php endforeach ?>

                                    </select>
                                </div>


                                <div class="form-group">
                                    <label>Catatan</label>
                                    <textarea type="text" rows="8" class="form-control" id="catatan" name="catatan"
                                        required></textarea>
                                </div>

                        </div>

                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Submit</button>
                    <a href="#" onclick="history.back();" class="btn btn-default float-right">Kembali</a>

                </div>
                </form>
            </div>
        </div>
    </div>

</section>
