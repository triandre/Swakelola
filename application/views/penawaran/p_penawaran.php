<?php

$mpdf = new \Mpdf\Mpdf([
    'mode' => 'utf-8',
    'format' => [210, 297],
    'orientation' => 'P'
]);

$isi =
    '<!DOCTYPE html>
<html>
<head>
    <title>Penawaran ' . $d["nama_pt"] . '</title>
    <style>@page {
        margin-top: 50px;
        margin-bottom: 20px;
        margin-right: 80px;
        margin-left: 80px;
       }</style>
</head>

<body>
<p align="right">Medan, ' . date('d F Y', strtotime($d['date_created'])) . ' </p>
<table>
<tbody>
<tr>
<td>Nomor</td>
<td>:</td>
<td>-</td>
</tr>
<tr>
<td>Lampiran</td>
<td>:</td>
<td>1 Berkas</td>
</tr>
<tr>
<td>Hal</td>
<td>:</td>
<td>Penawaran Harga</td>
</tr>
</tbody>
</table>
</p>
<br><br>
<p>Kepada Yth.<br>
<strong>Bapak Prof. Dr. Agussani M.AP</strong><br>
(Rektor Universitas Muhammadiyah Sumatera Utara)</p>

<p align="justify">Assalamualaikum Wr. Wb <br><br>
Semoga Bapak senantiasa dalam lindungan Allah Swt, sehat dan sukses melaksanakan tugas sehari-hari. Aamiin. <br><br>

Hormat kami, dengan ini saya sampaikan Surat Pengajuan <strong> ' . $d["judul_tender"] . ' </strong>(RAB dilampirkan) dengan perhitungan sebesar:</p>
<strong>Rp. ' . number_format($d["anggaran_penawaran"]) . ',-</strong> <strong>(<em>' . strtoupper(terbilang($d["anggaran_penawaran"])) . '  RUPIAH</em>)</strong>.
Demikian surat penawaran ini saya sampaikan, atas perhatian Bapak, saya ucapkan terima kasih.<br><br>
Wassalamualaikum Wr. Wb</p>
<br><br>
<table style="width:100%">
<tbody>
<tr>
<td style="width:50%"></td>
<td style="width:50%"><center>Hormat Saya,</center><br><br><br><br><br><br></td>
</tr>
<tr>
<td style="width:50%"></td>
<td style="width:50%"><center>' . $d["nama_pic"] . '</center></td>
</tr>
</tbody>
</table>




</body>
</html>';
$mpdf->WriteHTML($isi);
$mpdf->Output();
