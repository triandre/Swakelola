 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">
                 <h3 class="card-title">
                     Data Penawaran
                 </h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <?= $this->session->flashdata('message'); ?>
                 <table id="example1" class="table table-bordered table-striped">
                     <thead>
                         <tr>
                             <th>No</th>
                             <th>Instansi</th>
                             <th>Tender</th>
                             <th>Penawaran Harga</th>
                             <th>Status</th>
                             <th>#</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php $i = 1; ?>
                         <?php foreach ($v as $info) : ?>
                             <tr>
                                 <td><?= $i; ?></td>
                                 <td><a href="<?= base_url('detailProfil/') . $info['email']; ?>"> <?= $info['nama_pt']; ?>
                                     </a></td>
                                 <td><?= $info['judul_tender']; ?> <br>
                                     <p><small>
                                             Deskripsi : <?= $info['deskripsi_penawaran']; ?> <br>
                                             Tanggal Pengerjaan :
                                             <?= date('d F Y', strtotime($info['date_start_penawaran'])); ?> s/d
                                             <?= date('d F Y', strtotime($info['date_end_penawaran'])); ?> <br>
                                             Lampiran : <a href="<?= base_url('src/archive/') . $info['lampiran_penawaran']; ?>" target="_blank">Lihat</a>
                                         </small></p>
                                 </td>
                                 <td><?= number_format($info['anggaran_penawaran']) ?> </td>
                                 <td>
                                     <?php
                                        $today = date('Y-m-d');
                                        if ($info['sts_penawaran'] == 1) {
                                            echo '<span class="badge badge-info">Archive</span> ';
                                        } elseif ($info['sts_penawaran'] == 2) {
                                            echo '<span class="badge badge-success">Win</span>';
                                        } else {
                                            echo '<span class="badge badge-danger">Lose</span>';
                                        }
                                        ?>


                                 </td>

                                 <td>
                                     <div class="dropdown">
                                         <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                             <i class="fa fa-cog"></i>
                                         </button>
                                         <div class="dropdown-menu">
                                             <a href="<?= base_url('detailPenawaran/'); ?><?= $info['reff_penawaran']; ?>" class="dropdown-item">Detail Data</a>
                                             <a href="<?= base_url('cetakPenawaran/'); ?><?= $info['reff_penawaran']; ?>" class="dropdown-item" target="_blank">Cetak Penawaran</a>
                                             <?php if ($this->session->userdata('role_id') == 1) { ?>
                                                 <?php if ($info['sts_penawaran'] == 1) { ?>
                                                     <a href="<?= base_url('setujuiPenawaran/'); ?><?= $info['reff_penawaran']; ?>" class="dropdown-item">Setujui</a>
                                             <?php
                                                    }
                                                } ?>
                                         </div>
                                     </div>
                                 </td>
                             </tr>
                             <?php $i++; ?>
                         <?php endforeach; ?>
                     </tbody>
                 </table>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>

 </section>