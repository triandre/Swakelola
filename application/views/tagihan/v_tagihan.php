 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">
                 <h3 class="card-title">


                 </h3>
                 <a href="<?= base_url('c_tagihan'); ?>" onclick="history.back();" class="btn btn-danger"><i class="fa fa-plus"></i> Ajuan Baru</a>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <?= $this->session->flashdata('message'); ?>

                 <table id="" class="table ">
                     <thead>
                         <tr>
                             <th>No</th>
                             <th>Tanggal</th>
                             <th>Keterangan</th>
                             <th>Pengerjaan</th>
                             <th>Status</th>
                             <th>Lampiran</th>
                             <th>#</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php $i = 1; ?>
                         <?php foreach ($v as $info) : ?>
                             <tr>
                                 <td><?= $i; ?></td>
                                 <td><?= $info['created_at']; ?></td>
                                 <td><?= $info['catatan_pemohon']; ?></td>
                                 <td><?= $info['judul_tender']; ?> <br>
                                     <?php
                                        if ($info['sts_invoice'] == 5) { ?>
                                         <p style="color: chocolate;"><small><?= $info['ket_inv']; ?></small></p>
                                     <?php } ?>
                                 </td>
                                 <td>
                                     <?php
                                        if ($info['sts_invoice'] == 1) {
                                            echo '<span class="badge badge-info">Proses Validasi</span> ';
                                        } elseif ($info['sts_invoice'] == 2) {
                                            echo '<span class="badge badge-warning">Proses Pimpinan</span>';
                                        } elseif ($info['sts_invoice'] == 3) {
                                            echo '<span class="badge badge-primary">Setujui Pimpinan</span>';
                                        } elseif ($info['sts_invoice'] == 4) {
                                            echo '<span class="badge badge-success">Sudah Di bayar</span>';
                                        } elseif ($info['sts_invoice'] == 5) {
                                            echo '<span class="badge badge-danger">Ditolak</span>';
                                        } else {
                                            echo '<span class="badge badge-danger">Error</span>';
                                        }
                                        ?>


                                 </td>

                                 <td><a href="<?= base_url('src/archive/') . $info['lampiran_invoice']; ?>" target="_blank">Lihat</a> </td>
                                 <td>
                                     <div class="dropdown">
                                         <button type="button" class="btn-sm btn btn-info dropdown-toggle" data-toggle="dropdown">
                                             <i class="fa fa-cog"></i>
                                         </button>
                                         <div class="dropdown-menu">
                                             <a href="<?= base_url('d_tagihan/') . $info['reff_inv']; ?>" class="dropdown-item">Detail</a>
                                             <?php if ($this->session->userdata('role_id') == 2) { ?>
                                                 <?php if ($info['sts_invoice'] == 1) { ?>
                                                     <a href="<?= base_url('valid_tagihan/'); ?><?= $info['reff_inv']; ?>" class="dropdown-item">Validasi</a>
                                                     <a href="<?= base_url('tolak_tagihan/'); ?><?= $info['reff_inv']; ?>" class="dropdown-item">Tolak</a>
                                             <?php
                                                    }
                                                } ?>
                                             <?php if ($this->session->userdata('role_id') == 3) { ?>
                                                 <?php if ($info['sts_invoice'] == 5) { ?>
                                                     <a href="<?= base_url('revisi_tagihan/'); ?><?= $info['reff_inv']; ?>" class="dropdown-item">Perbaiki</a>
                                             <?php
                                                    }
                                                } ?>

                                             <?php if ($this->session->userdata('role_id') == 1) { ?>
                                                 <?php if ($info['sts_invoice'] == 2) { ?>
                                                     <a href="<?= base_url('acc_tagihan/'); ?><?= $info['reff_inv']; ?>" class="dropdown-item">Setujui</a>
                                                     <a href="<?= base_url('tolak_tagihan/'); ?><?= $info['reff_inv']; ?>" class="dropdown-item">Tolak</a>
                                             <?php
                                                    }
                                                } ?>
                                         </div>
                                     </div>
                                 </td>
                             </tr>
                             <?php $i++; ?>
                         <?php endforeach; ?>
                     </tbody>
                 </table>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>

 </section>