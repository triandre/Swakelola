<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    Invoice
                </h3>
            </div>
            <div class="card-body">
                <div class="card-body">
                    <form action="<?= base_url('validTagihanGo'); ?>" method="post">
                        <input hidden type="text" class="form-control" id="reff_inv" name="reff_inv" value="<?= $d['reff_inv']; ?>" readonly required>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">No. Invoice</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="no_inv" name="no_inv" value="<?= $d['no_inv']; ?>" readonly required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Pengerjaan</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="judul_tender" name="judul_tender" value="<?= $d['judul_tender']; ?>" readonly required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Transaksi</label>
                            <div class="col-sm-10">
                                <select type="text" class="form-control select2bs4" id="jenis_pembayaran" name="jenis_pembayaran" required>
                                    <option selected disabled value="">Pilih</option>
                                    <option value="Cash">Cash</option>
                                    <option value="Transfer">Transfer</option>

                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Tahapan</label>
                            <div class="col-sm-10">
                                <select type="text" class="form-control select2bs4" id="tahapan" name="tahapan" required>
                                    <option selected disabled value="">Pilih</option>
                                    <option value="Tahap 1">Tahap 1</option>
                                    <option value="Tahap 2">Tahap 2</option>
                                    <option value="Tahap 3">Tahap 3</option>
                                    <option value="Tahap 4">Tahap 4</option>
                                    <option value="Tahap 5">Tahap 5</option>
                                    <option value="Lunas">Lunas</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Persen</label>
                            <div class="col-sm-3">
                                <input type="number" class="form-control" id="persen" name="persen" value="<?= $d['persen']; ?>" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Nominal</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="duit" name="nominal" value="<?= $d['nominal']; ?>" required>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Catatan Untuk Pimpinan</label>
                            <div class="col-sm-10">
                                <textarea type="text" rows="5" class="form-control" id="ket_inv" name="ket_inv" required></textarea>
                            </div>
                        </div>

                </div>
                <div>
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Submit</button>
                    <a href="#" onclick="history.back();" class="btn btn-default float-right">Kembali</a>
                </div>
                </form>
            </div>
        </div>
    </div>

</section>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    Permohonan Tagihan
                </h3>
            </div>
            <div class="card-body">
                <div class="card-body">
                    <div class="row">
                        <div class="col-4">
                            <h3>Informasi</h3>
                            <hr>
                            <p>
                                Nama Ruangan: <br> <b><?= $d['nama_ruangan']; ?></b> <br>
                                Kriteria Pengadaan: <br> <b><?= $d['jenis_swakelola']; ?></b> <br>
                                Judul: <br> <b><?= $d['jenis_swakelola']; ?></b> <br>
                                Judul: <br> <b><?= $d['judul_tender']; ?></b> <br>
                                Keterangan: <br> <b><?= $d['keterangan']; ?></b> <br>
                                Pagu Anggaran: <br> <b>Rp. <?= number_format($d['pagu_anggaran']); ?></b> <br>
                                Target Selesai: <br> <b><?= date('d F Y', strtotime($d['date_end_work'])); ?></b>
                                <br>
                                Lampiran: <br> <b><a href="<?= base_url('src/archive/') . $d['lampiran'] ?>" target="_blank">Lihat
                                        file</a></b> <br>
                            </p>

                        </div>

                        <div class="col-4">
                            <h3>Penawaran Di Setujui</h3>
                            <hr>
                            <p> Instansi:
                                <br>
                                <b> <?= $d['nama_pt']; ?></b>
                                <br>
                                Total Penawaran:
                                <br>
                                <b>Rp. <?= number_format($d['anggaran_penawaran']); ?></b>
                                <br>
                                Deskripsi Penawaran:
                                <br>
                                <b><?= $d['deskripsi_penawaran']; ?></b>
                                <br>
                                Nama PIC:
                                <br>
                                <b><?= $d['nama_pic']; ?></b>
                                <br>
                                No. Hp PIC:
                                <br>
                                <b><?= $d['no_pic']; ?></b>
                                <br>
                                Mulai Pengerjaan:
                                <br>
                                <b><?= date('d F Y', strtotime($d['date_start_penawaran'])); ?></b>
                                <br>
                                Selesai Pengerjaan:
                                <br>
                                <b><?= date('d F Y', strtotime($d['date_end_penawaran'])); ?></b>
                                <br>
                                Lampiran Penawaran: <br>
                                <b><a href="<?= base_url('src/archive/') . $d['lampiran_penawaran'] ?>" target="_blank">Lihat
                                        file</a> </b>
                                <br>
                            </p>
                        </div>

                        <div class="col-4">
                            <h3>Catatan</h3>
                            <hr>
                            <p><b> <br>
                                    <?= $d['pembayaran']; ?> <br>
                                    <?= $d['ket_pimpinan']; ?></b></p>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

</section>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    Riwayat Pembayaran
                </h3>
            </div>
            <div class="card-body">
                <div class="card-body">

                </div>

            </div>
        </div>
    </div>

</section>