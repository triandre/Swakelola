<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    Detail Tagihan Diajukan
                </h3>
            </div>
            <div class="card-body">
                <div class="card-body">
                    <h3>Invoice</h3>
                    <hr>
                    <p>
                        Nama Ruangan: <br> <b><?= $d['nama_ruangan']; ?></b> <br>
                        Kriteria Pengadaan: <br> <b><?= $d['jenis_swakelola']; ?></b> <br>
                        Judul: <br> <b><?= $d['jenis_swakelola']; ?></b> <br>
                        Judul: <br> <b><?= $d['judul_tender']; ?></b> <br>
                        Keterangan: <br> <b><?= $d['keterangan']; ?></b> <br>
                        Pagu Anggaran: <br> <b>Rp. <?= number_format($d['pagu_anggaran']); ?></b> <br>
                        Target Selesai: <br> <b><?= date('d F Y', strtotime($d['date_end_work'])); ?></b>
                        <br>
                        Lampiran: <br> <b><a href="<?= base_url('src/archive/') . $d['lampiran'] ?>" target="_blank">Lihat
                                file</a></b> <br>
                    </p>

                </div>
                <div class="card-footer">
                    <a href="#" onclick="history.back();" class="btn btn-default float-right">Kembali</a>

                </div>
                </form>
            </div>
        </div>
    </div>

</section>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    Permohonan Tagihan
                </h3>
            </div>
            <div class="card-body">
                <div class="card-body">
                    <div class="row">
                        <div class="col-4">
                            <h3>Informasi</h3>
                            <hr>
                            <p>
                                Nama Ruangan: <br> <b><?= $d['nama_ruangan']; ?></b> <br>
                                Kriteria Pengadaan: <br> <b><?= $d['jenis_swakelola']; ?></b> <br>
                                Judul: <br> <b><?= $d['jenis_swakelola']; ?></b> <br>
                                Judul: <br> <b><?= $d['judul_tender']; ?></b> <br>
                                Keterangan: <br> <b><?= $d['keterangan']; ?></b> <br>
                                Pagu Anggaran: <br> <b>Rp. <?= number_format($d['pagu_anggaran']); ?></b> <br>
                                Target Selesai: <br> <b><?= date('d F Y', strtotime($d['date_end_work'])); ?></b>
                                <br>
                                Lampiran: <br> <b><a href="<?= base_url('src/archive/') . $d['lampiran'] ?>" target="_blank">Lihat
                                        file</a></b> <br>
                            </p>

                        </div>

                        <div class="col-4">
                            <h3>Penawaran Di Setujui</h3>
                            <hr>
                            <p> Instansi:
                                <br>
                                <b> <?= $d['nama_pt']; ?></b>
                                <br>
                                Total Penawaran:
                                <br>
                                <b>Rp. <?= number_format($d['anggaran_penawaran']); ?></b>
                                <br>
                                Deskripsi Penawaran:
                                <br>
                                <b><?= $d['deskripsi_penawaran']; ?></b>
                                <br>
                                Nama PIC:
                                <br>
                                <b><?= $d['nama_pic']; ?></b>
                                <br>
                                No. Hp PIC:
                                <br>
                                <b><?= $d['no_pic']; ?></b>
                                <br>
                                Mulai Pengerjaan:
                                <br>
                                <b><?= date('d F Y', strtotime($d['date_start_penawaran'])); ?></b>
                                <br>
                                Selesai Pengerjaan:
                                <br>
                                <b><?= date('d F Y', strtotime($d['date_end_penawaran'])); ?></b>
                                <br>
                                Lampiran Penawaran: <br>
                                <b><a href="<?= base_url('src/archive/') . $d['lampiran_penawaran'] ?>" target="_blank">Lihat
                                        file</a> </b>
                                <br>
                            </p>
                        </div>

                        <div class="col-4">
                            <h3>Catatan</h3>
                            <hr>
                            <p><b> <br>
                                    <?= $d['pembayaran']; ?> <br>
                                    <?= $d['ket_pimpinan']; ?></b></p>

                        </div>

                    </div>
                </div>
                <div class="card-footer">
                    <a href="#" onclick="history.back();" class="btn btn-default float-right">Kembali</a>

                </div>
                </form>
            </div>
        </div>
    </div>

</section>