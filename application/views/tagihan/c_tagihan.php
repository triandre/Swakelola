 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">
                 <h3 class="card-title">
                     Form Laporan Pengerjaan
                 </h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <form class="form-horizontal" method="post" action="<?= base_url('c_tagihanGo'); ?>" enctype="multipart/form-data">
                     <div class="card-body">

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Pengerjaan</label>
                             <div class="col-sm-10">
                                 <select type="text" class="form-control select2bs4" id="reff_penawaran" name="reff_penawaran" required>
                                     <option selected disabled value="">Pilih</option>
                                     <?php foreach ($tender as $tdr) : ?>
                                         <option value="<?= $tdr['reff_penawaran']; ?>"><?= $tdr['judul_tender']; ?></option>
                                     <?php endforeach ?>
                                 </select>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">catatan</label>
                             <div class="col-sm-10">
                                 <textarea type="text" rows="5" class="form-control" id="catatan_pemohon" name="catatan_pemohon" required></textarea>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Lampiran Rincian (Opsional)</label>
                             <div class="col-sm-2">
                                 <input type="file" class="form-control custom-file-input" id="lampiran_invoice" name="lampiran_invoice" accept="application/pdf, application/vnd.ms-excel">
                                 <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                             </div>
                         </div>
                     </div>
                     <!-- /.card-body -->
                     <div class="card-footer">
                         <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                         <a href="#" onclick="history.back();" class="btn btn-default float-right">Kembali</a>
                     </div>
                     <!-- /.card-footer -->
                 </form>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>
 </section>