 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">
                 <h3 class="card-title">
                     Ubah Password
                 </h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <form class="form-horizontal" method="post" action="<?= base_url('pengaturanPassword'); ?>"
                     enctype="multipart/form-data">
                     <div class="card-body">
                         <?= $this->session->flashdata('message'); ?>
                         <div class="row">
                             <div class="col-md-8">
                                 <div class="form-group">
                                     <label>Password Lama</label>
                                     <input type="password" class="form-control" id="current_password"
                                         name="current_password" required>
                                     <?= form_error('current_password', '<small class="text-danger pl-3">', '</small>'); ?>
                                 </div>
                             </div>
                         </div>
                         <div class="row">
                             <div class="col-md-8">
                                 <div class="form-group">
                                     <label>Password Baru</label>
                                     <input type="password" class="form-control" id="new_password1" minlength="6"
                                         name="new_password1" required>
                                     <?= form_error('new_password1', '<small class="text-danger pl-3">', '</small>'); ?>
                                 </div>
                             </div>
                         </div>
                         <div class="row">
                             <div class="col-md-8">
                                 <div class="form-group">
                                     <label>Ulangi Password Baru</label>
                                     <input type="password" class="form-control" minlength="6" id="new_password2"
                                         name="new_password2" required>
                                     <?= form_error('new_password2', '<small class="text-danger pl-3">', '</small>'); ?>
                                 </div>
                             </div>
                         </div>


                     </div>
                     <!-- /.card-body -->
                     <div class="card-footer">
                         <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                         <a href="#" onclick="history.back();" class="btn btn-default float-right">Kembali</a>
                     </div>
                     <!-- /.card-footer -->
                 </form>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>
 </section>