 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="row">
             <div class="col-md-3">

                 <!-- Profile Image -->
                 <div class="card card-primary card-outline">
                     <div class="card-body box-profile">
                         <div class="text-center">
                             <img class="profile-user-img img-fluid img-circle"
                                 src="<?= base_url('src/img/'). $this->session->userdata('image'); ?>"
                                 alt="User profile picture">
                         </div>

                         <h3 class="profile-username text-center"><?php echo $this->session->userdata('nama_pt'); ?>
                         </h3>

                         <p class="text-muted text-center"><?php echo $this->session->userdata('email'); ?></p>
                     </div>
                     <!-- /.card-body -->
                 </div>
                 <!-- /.card -->

                 <!-- About Me Box -->
                 <div class="card card-primary">
                     <div class="card-header">
                         <h3 class="card-title">About</h3>
                     </div>
                     <!-- /.card-header -->
                     <div class="card-body">
                         <strong><i class="fas fa-book mr-1"></i> No. HP</strong>
                         <p class="text-muted">
                             <?= $p['no_hp_pt'] ?>
                         </p>

                         <hr>

                         <strong><i class="fas fa-book mr-1"></i> No. Telpon</strong>
                         <p class="text-muted">
                             <?= $p['telpon_pt'] ?>
                         </p>
                         <hr>

                         <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>
                         <p class="text-muted"><?= $p['alamat'] ?></p>
                         <hr>

                         <strong><i class="fas fa-pencil-alt mr-1"></i> Bidang Usaha</strong>
                         <p class="text-muted"><?= $p['bidang_usaha'] ?></p>
                         <hr>

                         <strong><i class="far fa-file-alt mr-1"></i> Deskripsi</strong>

                         <p class="text-muted"><?= $p['deskripsi'] ?></p>
                     </div>
                     <!-- /.card-body -->
                 </div>
                 <!-- /.card -->
             </div>
             <!-- /.col -->
             <div class="col-md-9">
                 <div class="card">
                     <div class="card-header p-2">
                         <ul class="nav nav-pills">
                             <li class="nav-item"><a class="nav-link active" href="#profil" data-toggle="tab">Detail
                                     Profil</a>
                             </li>

                         </ul>
                     </div><!-- /.card-header -->
                     <div class="card-body">
                         <div class="tab-content">
                             <div class="active tab-pane" id="profil">
                                 <form class="form-horizontal" method="post"
                                     action="<?= base_url('updatePegaturanProfil'); ?>" enctype="multipart/form-data">
                                     <div class="form-group row">
                                         <label for="inputName" class="col-sm-2 col-form-label">Email</label>
                                         <div class="col-sm-10">
                                             <input type="email" class="form-control" id="email" name="email"
                                                 value="<?= $p['email']; ?>" required readonly>
                                         </div>
                                     </div>
                                     <div class="form-group row">
                                         <label for="inputEmail" class="col-sm-2 col-form-label">Nama</label>
                                         <div class="col-sm-10">
                                             <input type="text" class="form-control" id="nama_pt" name="nama_pt"
                                                 value="<?= $p['nama_pt']; ?>" required>
                                         </div>
                                     </div>
                                     <div class="form-group row">
                                         <label for="inputSkills" class="col-sm-2 col-form-label">NPWP</label>
                                         <div class="col-sm-10">
                                             <input type="text" class="form-control" id="npwp" name="npwp"
                                                 value="<?= $p['npwp']; ?>" required>
                                         </div>
                                     </div>
                                     <div class="form-group row">
                                         <label for="inputName2" class="col-sm-2 col-form-label">No. Telpon</label>
                                         <div class="col-sm-10">
                                             <input type="text" class="form-control" id="telpon_pt" name="telpon_pt"
                                                 value="<?= $p['telpon_pt']; ?>" required>
                                         </div>
                                     </div>
                                     <div class="form-group row">
                                         <label for="inputName2" class="col-sm-2 col-form-label">No. HP</label>
                                         <div class="col-sm-10">
                                             <input type="text" class="form-control" id="no_hp_pt" name="no_hp_pt"
                                                 value="<?= $p['no_hp_pt']; ?>" required>
                                         </div>
                                     </div>
                                     <div class="form-group row">
                                         <label for="inputExperience" class="col-sm-2 col-form-label">Bidang
                                             Usaha</label>
                                         <div class="col-sm-10">
                                             <input type="text" class="form-control" id="bidang_usaha"
                                                 name="bidang_usaha" value="<?= $p['bidang_usaha']; ?>" required>
                                         </div>
                                     </div>
                                     <div class="form-group row">
                                         <label for="inputSkills" class="col-sm-2 col-form-label">Nama PIC</label>
                                         <div class="col-sm-10">
                                             <input type="text" class="form-control" id="pic_pt" name="pic_pt"
                                                 value="<?= $p['pic_pt']; ?>" required>
                                         </div>
                                     </div>

                                     <div class="form-group row">
                                         <label for="inputSkills" class="col-sm-2 col-form-label">Provinsi</label>
                                         <div class="col-sm-10">
                                             <input type="text" class="form-control" id="provinsi" name="provinsi"
                                                 value="<?= $p['provinsi']; ?>" required>
                                         </div>
                                     </div>
                                     <div class="form-group row">
                                         <label for="inputSkills" class="col-sm-2 col-form-label">Kota</label>
                                         <div class="col-sm-10">
                                             <input type="text" class="form-control" id="kota" name="kota"
                                                 value="<?= $p['kota']; ?>" required>
                                         </div>
                                     </div>
                                     <div class="form-group row">
                                         <label for="inputSkills" class="col-sm-2 col-form-label">Kode Pos</label>
                                         <div class="col-sm-10">
                                             <input type="text" class="form-control" id="kode_pos" name="kode_pos"
                                                 value="<?= $p['kode_pos']; ?>" required>
                                         </div>
                                     </div>
                                     <div class="form-group row">
                                         <label for="inputSkills" class="col-sm-2 col-form-label">alamat</label>
                                         <div class="col-sm-10">
                                             <input type="text" class="form-control" id="alamat" name="alamat"
                                                 value="<?= $p['alamat']; ?>" required>
                                         </div>
                                     </div>

                                     <div class="form-group row">
                                         <label for="inputSkills" class="col-sm-2 col-form-label">Photo</label>
                                         <div class="col-sm-10">
                                             <input type="file" class="form-control" id="image" name="image"
                                                 accept="image/*">
                                         </div>
                                     </div>

                                     <div class="form-group row">
                                         <div class="offset-sm-2 col-sm-10">
                                             <button type="submit" class="btn btn-danger">Save/Update</button>
                                         </div>
                                     </div>
                                 </form>
                             </div>

                         </div>
                         <!-- /.tab-content -->

                     </div><!-- /.card-body -->
                 </div>
                 <!-- /.card -->
             </div>
             <!-- /.col -->

         </div>
         <!-- /.row -->
     </div>

     <!-- /.container-fluid -->
 </section>