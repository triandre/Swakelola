 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="row">
             <div class="col-md-3">

                 <!-- Profile Image -->
                 <div class="card card-primary card-outline">
                     <div class="card-body box-profile">
                         <div class="text-center">
                             <img class="profile-user-img img-fluid img-circle"
                                 src="<?= base_url('src/img/'). $this->session->userdata('image'); ?>"
                                 alt="User profile picture">
                         </div>

                         <h3 class="profile-username text-center"><?php echo $this->session->userdata('nama_pt'); ?>
                         </h3>

                         <p class="text-muted text-center"><?php echo $this->session->userdata('email'); ?></p>
                     </div>
                     <!-- /.card-body -->
                 </div>
                 <!-- /.card -->

                 <!-- About Me Box -->
                 <div class="card card-primary">
                     <div class="card-header">
                         <h3 class="card-title">About</h3>
                     </div>
                     <!-- /.card-header -->
                     <div class="card-body">
                         <strong><i class="fas fa-book mr-1"></i> No. HP</strong>
                         <p class="text-muted">
                             <?= $p['no_hp_pt'] ?>
                         </p>

                         <hr>

                         <strong><i class="fas fa-book mr-1"></i> No. Telpon</strong>
                         <p class="text-muted">
                             <?= $p['telpon_pt'] ?>
                         </p>
                         <hr>

                         <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>
                         <p class="text-muted"><?= $p['alamat'] ?></p>
                         <hr>

                         <strong><i class="fas fa-pencil-alt mr-1"></i> Bidang Usaha</strong>
                         <p class="text-muted"><?= $p['bidang_usaha'] ?></p>
                         <hr>

                         <strong><i class="far fa-file-alt mr-1"></i> Deskripsi</strong>

                         <p class="text-muted"><?= $p['deskripsi'] ?></p>
                     </div>
                     <!-- /.card-body -->
                 </div>
                 <!-- /.card -->
             </div>
             <!-- /.col -->
             <div class="col-md-9">
                 <div class="card">
                     <div class="card-header p-2">
                         <ul class="nav nav-pills">
                             <li class="nav-item"><a class="nav-link" href="<?= base_url('profil'); ?>">Profil</a>
                             </li>
                             <li class="nav-item"><a class="nav-link" href="<?= base_url('user/akta'); ?>">Akta</a>
                             </li>
                             <li class="nav-item"><a class="nav-link active"
                                     href="<?= base_url('user/struktur'); ?>">Struktur</a>
                             </li>
                             <li class="nav-item"><a class="nav-link"
                                     href="<?= base_url('user/fotoKantor'); ?>">Kantor</a>
                             </li>
                         </ul>
                     </div><!-- /.card-header -->
                     <div class="card-body">
                         <div class="tab-content">

                             <div class="active tab-pane" id="timeline">
                                 <a data-toggle="modal" data-target="#newArchive" class="btn-sm btn-danger"> <i
                                         class="fa fa-plus"></i> Tambah Data </a>
                                 <table id="example1" class="table table-bordered table-striped">
                                     <thead>
                                         <tr>
                                             <th>No</th>
                                             <th>Nama</th>
                                             <th>Jabatan</th>
                                             <th>No. Hp</th>
                                             <th>Email</th>
                                             <th>#</th>
                                         </tr>
                                     </thead>
                                     <tbody>
                                         <?php $no = 1; ?>
                                         <?php foreach ($vs as $i) :
											$id_struktur = $i['id_struktur'];
											$nama_pegawai = $i['nama_pegawai'];
											$jabatan = $i['jabatan'];
											$no_hp = $i['no_hp'];
											$email_pegawai = $i['email_pegawai'];
											?>
                                         <tr>
                                             <td><?php echo $no; ?></td>
                                             <td><?php echo $nama_pegawai; ?></td>
                                             <td><?php echo $jabatan; ?></td>
                                             <td><?php echo $no_hp; ?></td>
                                             <td><?php echo $email_pegawai; ?></td>
                                             <td>
                                                 <div class="dropdown">
                                                     <button type="button" class="btn btn-primary dropdown-toggle"
                                                         data-toggle="dropdown">
                                                         <i class="fa fa-cog"></i>
                                                     </button>
                                                     <div class="dropdown-menu">
                                                         <a href="" data-toggle="modal"
                                                             data-target="#editArchive<?php echo $id_struktur; ?>"
                                                             class="dropdown-item">Ubah data</a>
                                                         <a href="<?= base_url('user/hapusStruktur/'); ?><?php echo $id_struktur; ?>"
                                                             class="dropdown-item tombol-hapus">Hapus Data</a>
                                                     </div>
                                                 </div>
                                             </td>

                                         </tr>
                                         <?php $no++; ?>
                                         <?php endforeach; ?>
                                     </tbody>

                                 </table>
                             </div>

                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </section>

 <!-- New Archive Modal-->
 <div class="modal fade" id="newArchive" tabindex="-1" role="dialog" aria-labelledby="newArchive" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="newArchive">Struktur</h5>
                 <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">×</span>
                 </button>
             </div>
             <div class="modal-body">
                 <form class="form-horizontal" method="post" action="<?= base_url('user/tambahStruktur'); ?>"
                     enctype="multipart/form-data">

                     <div class="form-group row">
                         <label for="inputEmail" class="col-sm-2 col-form-label">Nama</label>
                         <div class="col-sm-10">
                             <input type="text" class="form-control" id="nama_pegawai" name="nama_pegawai" required>
                         </div>
                     </div>
                     <div class="form-group row">
                         <label for="inputSkills" class="col-sm-2 col-form-label">Jabatan</label>
                         <div class="col-sm-10">
                             <input type="text" class="form-control" id="jabatan" name="jabatan" required>
                         </div>
                     </div>
                     <div class="form-group row">
                         <label for="inputName2" class="col-sm-2 col-form-label">No. Hp</label>
                         <div class="col-sm-10">
                             <input type="text" class="form-control" id="no_hp" name="no_hp" required>
                         </div>
                     </div>
                     <div class="form-group row">
                         <label for="inputName2" class="col-sm-2 col-form-label">Email</label>
                         <div class="col-sm-10">
                             <input type="email" class="form-control" id="email_pegawai" name="email_pegawai" required>
                         </div>
                     </div>
             </div>
             <div class="modal-footer">
                 <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                 <button class="btn btn-primary" type="submit">Save</button>
                 </form>
             </div>
         </div>
     </div>
 </div>

 <?php foreach ($vs as $i) :
  $id_struktur = $i['id_struktur'];
  $nama_pegawai = $i['nama_pegawai'];
  $jabatan = $i['jabatan'];
  $no_hp = $i['no_hp'];
  $email_pegawai = $i['email_pegawai'];
  ?>
 <!-- Edit Archive Modal-->
 <div class="modal fade" id="editArchive<?php echo $id_struktur; ?>" tabindex="-1" role="dialog"
     aria-labelledby="editArchive" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="newArchive">Edit Data</h5>
                 <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">×</span>
                 </button>
             </div>
             <div class="modal-body">
                 <form class="form-horizontal" method="post" action="<?= base_url('user/ubahStruktur'); ?>"
                     enctype="multipart/form-data">
                     <input hidden type="text" class="form-control" id="id_struktur" name="id_struktur"
                         value="<?php echo $id_struktur ?>" readonly required>
                     <div class="form-group row">
                         <label for="inputEmail" class="col-sm-2 col-form-label">Nama</label>
                         <div class="col-sm-10">
                             <input type="text" class="form-control" id="nama_pegawai" name="nama_pegawai"
                                 value="<?php echo $nama_pegawai ?>" required>
                         </div>
                     </div>
                     <div class=" form-group row">
                         <label for="inputSkills" class="col-sm-2 col-form-label">Jabatan</label>
                         <div class="col-sm-10">
                             <input type="text" class="form-control" id="jabatan" name="jabatan"
                                 value="<?php echo $jabatan ?>" required>
                         </div>
                     </div>
                     <div class=" form-group row">
                         <label for="inputName2" class="col-sm-2 col-form-label">No. Hp</label>
                         <div class="col-sm-10">
                             <input type="text" class="form-control" id="no_hp" name="no_hp"
                                 value="<?php echo $no_hp ?>" required>
                         </div>
                     </div>
                     <div class=" form-group row">
                         <label for="inputName2" class="col-sm-2 col-form-label">Email</label>
                         <div class="col-sm-10">
                             <input type="email" class="form-control" id="email_pegawai" name="email_pegawai"
                                 value="<?php echo $email_pegawai ?>" required>
                         </div>
                     </div>
             </div>
             <div class=" modal-footer">
                 <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                 <button class="btn btn-primary" type="submit">Save</button>
                 </form>
             </div>
         </div>
     </div>
 </div>
 <?php endforeach; ?>
