 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="row">
             <div class="col-md-3">

                 <!-- Profile Image -->
                 <div class="card card-primary card-outline">
                     <div class="card-body box-profile">
                         <div class="text-center">
                             <img class="profile-user-img img-fluid img-circle"
                                 src="<?= base_url('src/img/'). $this->session->userdata('image'); ?>"
                                 alt="User profile picture">
                         </div>

                         <h3 class="profile-username text-center"><?php echo $this->session->userdata('nama_pt'); ?>
                         </h3>

                         <p class="text-muted text-center"><?php echo $this->session->userdata('email'); ?></p>
                     </div>
                     <!-- /.card-body -->
                 </div>
                 <!-- /.card -->

                 <!-- About Me Box -->
                 <div class="card card-primary">
                     <div class="card-header">
                         <h3 class="card-title">About</h3>
                     </div>
                     <!-- /.card-header -->
                     <div class="card-body">
                         <strong><i class="fas fa-book mr-1"></i> No. HP</strong>
                         <p class="text-muted">
                             <?= $p['no_hp_pt'] ?>
                         </p>

                         <hr>

                         <strong><i class="fas fa-book mr-1"></i> No. Telpon</strong>
                         <p class="text-muted">
                             <?= $p['telpon_pt'] ?>
                         </p>
                         <hr>

                         <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>
                         <p class="text-muted"><?= $p['alamat'] ?></p>
                         <hr>

                         <strong><i class="fas fa-pencil-alt mr-1"></i> Bidang Usaha</strong>
                         <p class="text-muted"><?= $p['bidang_usaha'] ?></p>
                         <hr>

                         <strong><i class="far fa-file-alt mr-1"></i> Deskripsi</strong>

                         <p class="text-muted"><?= $p['deskripsi'] ?></p>
                     </div>
                     <!-- /.card-body -->
                 </div>
                 <!-- /.card -->
             </div>
             <!-- /.col -->
             <div class="col-md-9">
                 <div class="card">
                     <div class="card-header p-2">
                         <ul class="nav nav-pills">
                             <li class="nav-item"><a class="nav-link" href="<?= base_url('profil'); ?>">Profil</a>
                             </li>
                             <li class="nav-item"><a class="nav-link active"
                                     href="<?= base_url('user/akta'); ?>">Akta</a>
                             </li>
                             <li class="nav-item"><a class="nav-link"
                                     href="<?= base_url('user/struktur'); ?>">Struktur</a>
                             </li>
                             <li class="nav-item"><a class="nav-link"
                                     href="<?= base_url('user/fotoKantor'); ?>">Kantor</a>
                             </li>
                         </ul>
                     </div><!-- /.card-header -->
                     <div class="card-body">
                         <div class="tab-content">

                             <div class="active tab-pane" id="activity">
                                 <form class="form-horizontal" method="post" action="<?= base_url('updateAkta'); ?>"
                                     enctype="multipart/form-data">
                                     <div class="form-group row">
                                         <label for="inputName" class="col-sm-2 col-form-label">Email</label>
                                         <div class="col-sm-10">
                                             <input type="email" class="form-control" id="email" name="email"
                                                 value="<?= $p['email']; ?>" required readonly>
                                         </div>
                                     </div>
                                     <div class="form-group row">
                                         <label for="inputEmail" class="col-sm-2 col-form-label">Nama Pada Akta</label>
                                         <div class="col-sm-10">
                                             <input type="text" class="form-control" id="nama_pt_akta"
                                                 name="nama_pt_akta" value="<?= $p['nama_pt_akta']; ?>" required>
                                         </div>
                                     </div>
                                     <div class="form-group row">
                                         <label for="inputName2" class="col-sm-2 col-form-label">No. Akta</label>
                                         <div class="col-sm-10">
                                             <input type="text" class="form-control" id="no_akta" name="no_akta"
                                                 value="<?= $p['no_akta']; ?>" required>
                                         </div>
                                     </div>
                                     <div class="form-group row">
                                         <label for="inputExperience" class="col-sm-2 col-form-label">Tanggal
                                             Akta</label>
                                         <div class="col-sm-10">
                                             <input type="date" class="form-control" id="tgl_akta" name="tgl_akta"
                                                 value="<?= $p['tgl_akta']; ?>" required>
                                         </div>
                                     </div>
                                     <div class="form-group row">
                                         <label for="inputSkills" class="col-sm-2 col-form-label">Nama Notaris</label>
                                         <div class="col-sm-10">
                                             <input type="text" class="form-control" id="nama_notaris"
                                                 name="nama_notaris" value="<?= $p['nama_notaris']; ?>" required>
                                         </div>
                                     </div>

                                     <div class="form-group row">
                                         <label for="inputSkills" class="col-sm-2 col-form-label">File Akta</label>
                                         <div class="col-sm-5">
                                             <label for="inputSkills" class="col-sm-2 col-form-label"><a
                                                     href="<?= base_url('src/archive/').$p['file_akta']; ?>">
                                                     Lihat</a></label>
                                         </div>
                                         <div class="col-sm-5">
                                             <input type="file" class="form-control custom-file-input" id="file_akta"
                                                 name="file_akta" accept="application/pdf, application/vnd.ms-excel"
                                                 required>
                                             <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                         </div>
                                     </div>

                                     <div class="form-group row">
                                         <div class="offset-sm-2 col-sm-10">
                                             <button type="submit" class="btn btn-danger">Save/Update</button>
                                         </div>
                                     </div>
                                 </form>
                             </div>


                             <!-- /.tab-pane -->
                         </div>
                         <!-- /.tab-content -->

                     </div><!-- /.card-body -->
                 </div>
                 <!-- /.card -->
             </div>
             <!-- /.col -->

         </div>
         <!-- /.row -->
     </div><!-- /.container-fluid -->
 </section>