<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $title; ?></title>
    <link rel="shortcut icon" type="image/icon" href="<?= base_url('src/img/px.png'); ?>">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url('src/thm/'); ?>plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('src/thm/'); ?>dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->

    <!-- Select2 -->
    <link rel="stylesheet" href="<?= base_url('src/thm/'); ?>plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?= base_url('src/thm/'); ?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="<?= base_url('src/thm/'); ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?= base_url('src/thm/'); ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="<?= base_url('src/thm/'); ?>plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="<?= base_url('src/thm/'); ?>plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">


        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="index3.html" class="nav-link">Home</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="#" class="nav-link">Contact</a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <!-- Navbar Search -->
                <li class="nav-item">
                    <a class="nav-link" data-widget="navbar-search" href="#" role="button">
                        <i class="fas fa-search"></i>
                    </a>
                    <div class="navbar-search-block">
                        <form class="form-inline">
                            <div class="input-group input-group-sm">
                                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                                <div class="input-group-append">
                                    <button class="btn btn-navbar" type="submit">
                                        <i class="fas fa-search"></i>
                                    </button>
                                    <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </li>

                <!-- Messages Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="far fa-comments"></i>
                        <span class="badge badge-danger navbar-badge">3</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <a href="#" class="dropdown-item">
                            <!-- Message Start -->
                            <div class="media">
                                <img src="dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        Brad Diesel
                                        <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                                    </h3>
                                    <p class="text-sm">Call me whenever you can...</p>
                                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                                </div>
                            </div>
                            <!-- Message End -->
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <!-- Message Start -->
                            <div class="media">
                                <img src="dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        John Pierce
                                        <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                                    </h3>
                                    <p class="text-sm">I got your message bro</p>
                                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                                </div>
                            </div>
                            <!-- Message End -->
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <!-- Message Start -->
                            <div class="media">
                                <img src="dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        Nora Silvester
                                        <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                                    </h3>
                                    <p class="text-sm">The subject goes here</p>
                                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                                </div>
                            </div>
                            <!-- Message End -->
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
                    </div>
                </li>
                <!-- Notifications Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="far fa-bell"></i>
                        <span class="badge badge-warning navbar-badge">15</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-item dropdown-header">15 Notifications</span>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-envelope mr-2"></i> 4 new messages
                            <span class="float-right text-muted text-sm">3 mins</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-users mr-2"></i> 8 friend requests
                            <span class="float-right text-muted text-sm">12 hours</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-file mr-2"></i> 3 new reports
                            <span class="float-right text-muted text-sm">2 days</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                        <i class="fas fa-expand-arrows-alt"></i>
                    </a>
                </li>

            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="#" class="brand-link">
                <img src="<?= base_url('src/thm/'); ?>dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">SwaKelola UMSU</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src=" <?= base_url('src'); ?>/img/<?php echo $this->session->userdata('image'); ?>" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block"> <?php echo $this->session->userdata('nama_pt'); ?></a>
                    </div>
                </div>

                <?php
                if ($this->session->userdata('role_id') == 1) { ?>
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            <li class="nav-item">
                                <a href="<?= base_url('pimpinan'); ?>" class="nav-link <?= isset($active_menu_db) ? $active_menu_db : '' ?>">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p>
                                        Dashboard
                                    </p>
                                </a>

                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('informasi'); ?>" class="nav-link <?= isset($active_menu_vinfo) ? $active_menu_vinfo : '' ?>">
                                    <i class="nav-icon fas fa-th"></i>
                                    <p>
                                        Informasi Tender
                                    </p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('penawaranAdmin'); ?>" class="nav-link <?= isset($active_menu_vpen) ? $active_menu_vpen : '' ?>">
                                    <i class="nav-icon fas fa-file"></i>
                                    <p>
                                        Penawaran Harga
                                    </p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('pemenang'); ?>" class="nav-link <?= isset($active_menu_vmenang) ? $active_menu_vmenang : '' ?>">
                                    <i class="nav-icon fas fa-archive"></i>
                                    <p>
                                        Pemenang Tender
                                    </p>
                                </a>
                            </li>


                            <li class="nav-item <?= isset($active_menu_tagihan) ? $active_menu_tagihan : '' ?>">
                                <a href="#" class="nav-link <?= isset($active_menu_tgh) ? $active_menu_tgh : '' ?>">
                                    <i class="nav-icon fas fa-envelope"></i>
                                    <p>
                                        Permohonan Tagihan
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">

                                    <li class="nav-item">
                                        <a href="<?= base_url('v_tagihan'); ?>" class="nav-link <?= isset($active_menu_vtg) ? $active_menu_vtg : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Permohonan Tagihan</p>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a href="<?= base_url('r_tagihan'); ?>" class="nav-link <?= isset($active_menu_rtg) ? $active_menu_rtg : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Riwayat pembayaran</p>
                                        </a>
                                    </li>


                                    <li class="nav-item">
                                        <a href="<?= base_url('l_tagihan'); ?>" class="nav-link <?= isset($active_menu_laptg) ? $active_menu_laptg : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Laporan pembayaran</p>
                                        </a>
                                    </li>

                                </ul>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('lapkerAdmin'); ?>" class="nav-link <?= isset($active_menu_lapkerAdmin) ? $active_menu_lapkerAdmin : '' ?>">
                                    <i class="nav-icon fas fa-print"></i>
                                    <p>
                                        Laporan Kerja
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('maps'); ?>" class="nav-link <?= isset($active_menu_maps) ? $active_menu_maps : '' ?>">
                                    <i class="nav-icon fas fa-map"></i>
                                    <p>
                                        Maps
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item <?= isset($active_menu_pengaturan) ? $active_menu_pengaturan : '' ?>">
                                <a href="#" class="nav-link <?= isset($active_menu_prt) ? $active_menu_prt : '' ?>">
                                    <i class="nav-icon fas fa-cog"></i>
                                    <p>
                                        Pengaturan
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="<?= base_url('pengaturanProfil') ?>" class="nav-link <?= isset($active_menu_vpp) ? $active_menu_vpp : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Profil</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?= base_url('pengaturanPassword') ?>" class="nav-link <?= isset($active_menu_ppswd) ? $active_menu_ppswd : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Ubah Password</p>
                                        </a>
                                    </li>

                                </ul>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('logout') ?>" class="nav-link">
                                    <i class="nav-icon fas fa-sign-out-alt"></i>
                                    <p>
                                        Logout
                                    </p>
                                </a>
                            </li>




                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                <?php } ?>



                <!-- Sidebar Menu -->
                <?php
                if ($this->session->userdata('role_id') == 2) { ?>
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            <li class="nav-item">
                                <a href="<?= base_url('admin'); ?>" class="nav-link <?= isset($active_menu_db) ? $active_menu_db : '' ?>">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p>
                                        Dashboard
                                    </p>
                                </a>

                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('informasi'); ?>" class="nav-link <?= isset($active_menu_vinfo) ? $active_menu_vinfo : '' ?>">
                                    <i class="nav-icon fas fa-th"></i>
                                    <p>
                                        Informasi Tender
                                    </p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('penawaranAdmin'); ?>" class="nav-link <?= isset($active_menu_vpen) ? $active_menu_vpen : '' ?>">
                                    <i class="nav-icon fas fa-file"></i>
                                    <p>
                                        Penawaran Harga
                                    </p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('pemenang'); ?>" class="nav-link <?= isset($active_menu_vmenang) ? $active_menu_vmenang : '' ?>">
                                    <i class="nav-icon fas fa-archive"></i>
                                    <p>
                                        Pemenang Tender
                                    </p>
                                </a>
                            </li>

                            <li class="nav-item <?= isset($active_menu_tagihan) ? $active_menu_tagihan : '' ?>">
                                <a href="#" class="nav-link <?= isset($active_menu_tgh) ? $active_menu_tgh : '' ?>">
                                    <i class="nav-icon fas fa-envelope"></i>
                                    <p>
                                        Permohonan Tagihan
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">

                                    <li class="nav-item">
                                        <a href="<?= base_url('v_tagihan'); ?>" class="nav-link <?= isset($active_menu_vtg) ? $active_menu_vtg : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Permohonan Tagihan</p>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a href="<?= base_url('r_tagihan'); ?>" class="nav-link <?= isset($active_menu_rtg) ? $active_menu_rtg : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Riwayat pembayaran</p>
                                        </a>
                                    </li>


                                    <li class="nav-item">
                                        <a href="<?= base_url('l_tagihan'); ?>" class="nav-link <?= isset($active_menu_laptg) ? $active_menu_laptg : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Laporan pembayaran</p>
                                        </a>
                                    </li>

                                </ul>
                            </li>



                            <li class="nav-item">
                                <a href="<?= base_url('invoice'); ?>" class="nav-link <?= isset($active_menu_vinvoice) ? $active_menu_vinvoice : '' ?>">
                                    <i class="nav-icon fas fa-barcode"></i>
                                    <p>
                                        Invoice
                                    </p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('lapkerAdmin'); ?>" class="nav-link <?= isset($active_menu_lapkerAdmin) ? $active_menu_lapkerAdmin : '' ?>">
                                    <i class="nav-icon fas fa-print"></i>
                                    <p>
                                        Laporan Kerja
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('maps'); ?>" class="nav-link <?= isset($active_menu_maps) ? $active_menu_maps : '' ?>">
                                    <i class="nav-icon fas fa-map"></i>
                                    <p>
                                        Maps
                                    </p>
                                </a>
                            </li>


                            <li class="nav-header">MANAJEMEN</li>
                            <li class="nav-item">
                                <a href="<?= base_url('v_user'); ?>" class="nav-link <?= isset($active_menu_vuser) ? $active_menu_vuser : '' ?>">
                                    <i class="nav-icon far fa-user"></i>
                                    <p>
                                        Users management
                                    </p>
                                </a>
                            </li>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('v_vendor'); ?>" class="nav-link <?= isset($active_menu_vendor) ? $active_menu_vendor : '' ?>">
                                    <i class="nav-icon far fa-user"></i>
                                    <p>
                                        Vendor
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('v_swa') ?>" class="nav-link <?= isset($active_menu_vswa) ? $active_menu_vswa : '' ?>">
                                    <i class="nav-icon fas fa-columns"></i>
                                    <p>
                                        Swakelola
                                    </p>
                                </a>
                            </li>

                            <li class="nav-item <?= isset($active_menu_bar) ? $active_menu_bar : '' ?>">
                                <a href="#" class="nav-link <?= isset($active_menu_br) ? $active_menu_br : '' ?>">
                                    <i class="nav-icon fas fa-bars"></i>
                                    <p>
                                        Lokasi Aset
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="<?= base_url('v_kantor') ?>" class="nav-link <?= isset($active_menu_vkantor) ? $active_menu_vkantor : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Area</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?= base_url('v_aset') ?>" class="nav-link <?= isset($active_menu_vaset) ? $active_menu_vaset : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Gedung</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?= base_url('v_ruangan') ?>" class="nav-link <?= isset($active_menu_vruangan) ? $active_menu_vruangan : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Ruangan</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item <?= isset($active_menu_pengaturan) ? $active_menu_pengaturan : '' ?>">
                                <a href="#" class="nav-link <?= isset($active_menu_prt) ? $active_menu_prt : '' ?>">
                                    <i class="nav-icon fas fa-cog"></i>
                                    <p>
                                        Pengaturan
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="<?= base_url('pengaturanProfil') ?>" class="nav-link <?= isset($active_menu_vpp) ? $active_menu_vpp : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Profil</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?= base_url('pengaturanPassword') ?>" class="nav-link <?= isset($active_menu_ppswd) ? $active_menu_ppswd : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Ubah Password</p>
                                        </a>
                                    </li>

                                </ul>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('logout') ?>" class="nav-link">
                                    <i class="nav-icon fas fa-sign-out-alt"></i>
                                    <p>
                                        Logout
                                    </p>
                                </a>
                            </li>




                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                <?php } ?>

                <?php
                if ($this->session->userdata('role_id') == 3) { ?>
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            <li class="nav-item">
                                <a href="<?= base_url('user'); ?>" class="nav-link <?= isset($active_menu_db) ? $active_menu_db : '' ?>">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p>
                                        Dashboard
                                    </p>
                                </a>

                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('profil'); ?>" class="nav-link <?= isset($active_menu_profil) ? $active_menu_profil : '' ?>">
                                    <i class="nav-icon fas fa-user"></i>
                                    <p>
                                        Profil
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('informasi'); ?>" class="nav-link <?= isset($active_menu_vinfo) ? $active_menu_vinfo : '' ?>">
                                    <i class="nav-icon fas fa-th"></i>
                                    <p>
                                        Informasi Tender
                                    </p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('penawaran'); ?>" class="nav-link <?= isset($active_menu_vpen) ? $active_menu_vpen : '' ?>">
                                    <i class="nav-icon fas fa-file"></i>
                                    <p>
                                        Penawaran Harga
                                    </p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('lapker'); ?>" class="nav-link <?= isset($active_menu_lapker) ? $active_menu_lapker : '' ?>">
                                    <i class="nav-icon fas fa-print"></i>
                                    <p>
                                        Laporan Kerja
                                    </p>
                                </a>
                            </li>

                            <li class="nav-item <?= isset($active_menu_tagihan) ? $active_menu_tagihan : '' ?>">
                                <a href="#" class="nav-link <?= isset($active_menu_tgh) ? $active_menu_tgh : '' ?>">
                                    <i class="nav-icon fas fa-envelope"></i>
                                    <p>
                                        Permohonan Tagihan
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">

                                    <li class="nav-item">
                                        <a href="<?= base_url('c_tagihan'); ?>" class="nav-link <?= isset($active_menu_ctg) ? $active_menu_ctg : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p> Buat Permohonan</p>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a href="<?= base_url('v_tagihan'); ?>" class="nav-link <?= isset($active_menu_vtg) ? $active_menu_vtg : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Permohonan Tagihan</p>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a href="<?= base_url('r_tagihan'); ?>" class="nav-link <?= isset($active_menu_rtg) ? $active_menu_rtg : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Riwayat pembayaran</p>
                                        </a>
                                    </li>


                                    <li class="nav-item">
                                        <a href="<?= base_url('l_tagihan'); ?>" class="nav-link <?= isset($active_menu_laptg) ? $active_menu_laptg : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Laporan pembayaran</p>
                                        </a>
                                    </li>

                                </ul>
                            </li>




                            <li class="nav-item <?= isset($active_menu_pengaturan) ? $active_menu_pengaturan : '' ?>">
                                <a href="#" class="nav-link <?= isset($active_menu_prt) ? $active_menu_prt : '' ?>">
                                    <i class="nav-icon fas fa-cog"></i>
                                    <p>
                                        Pengaturan
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">

                                    <li class="nav-item">
                                        <a href="<?= base_url('pengaturanPassword') ?>" class="nav-link <?= isset($active_menu_ppswd) ? $active_menu_ppswd : '' ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Ubah Password</p>
                                        </a>
                                    </li>

                                </ul>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('logout') ?>" class="nav-link">
                                    <i class="nav-icon fas fa-sign-out-alt"></i>
                                    <p>
                                        Logout
                                    </p>
                                </a>
                            </li>




                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                <?php } ?>
            </div>
            <!-- /.sidebar -->
        </aside>
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0"><?= $title; ?></h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                                <li class="breadcrumb-item active"><?= $title; ?></li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->


            <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
            <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>