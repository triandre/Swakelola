</div>
<footer class="main-footer">
    <strong>Copyright &copy; <?= date('Y') ?><a href=""> Swakelola UMSU</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1.0.0
    </div>
</footer>
<aside class="control-sidebar control-sidebar-dark">
</aside>
</div>

<script src="<?= base_url('src/thm/'); ?>plugins/jquery/jquery.min.js"></script>
<script src="<?= base_url('src/thm/'); ?>plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?= base_url('src/thm/'); ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url('src/thm/'); ?>dist/js/adminlte.min.js"></script>

<script src="<?= base_url('src/thm/'); ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('src/thm/'); ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url('src/thm/'); ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url('src/thm/'); ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

<script src="<?= base_url('src/thm/'); ?>plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="<?= base_url('src/sweetalert/'); ?>notif.js"></script>
<script src="<?= base_url('src/sweetalert/'); ?>duit.js"></script>
<script src="<?= base_url('src/thm/'); ?>plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="<?= base_url('src/thm/'); ?>plugins/select2/js/select2.full.min.js"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false
        });

        $("#example2").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false
        });

        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })


    });
</script>
<script type="text/javascript">
    function setMapToForm(latitude, longitude) {
        $('input[name="latitude"]').val(latitude);
        $('input[name="longitude"]').val(longitude);
    }
</script>
</body>

</html>