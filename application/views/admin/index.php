<?php
$today = date('Y-m-d');
$jumTender = $this->db->query(" SELECT * 
    FROM swa_tender 
    WHERE active_tender = '1' 
    AND '$today' >= date_start_tender 
    AND '$today' <= date_end_tender");
$jumPen = $this->db->query("SELECT * FROM swa_penawaran where sts_penawaran='1'");
$jumVen = $this->db->query("SELECT * FROM swa_user where role_id='3'");
?>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3><?php echo $jumVen->num_rows(); ?></h3>

                        <p>Vendor</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-stalker"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3><?php echo $jumTender->num_rows(); ?></h3>

                        <p>Tender</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-folder"></i>
                    </div>
                    <a href="<?= base_url('informasi'); ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3><?php echo $jumPen->num_rows(); ?></h3>

                        <p>Penawaran</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-document-text"></i>
                    </div>
                    <a href="<?= base_url('penawaran'); ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3>65</h3>

                        <p>Dalam Pengerjaan</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-settings"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    <span class="badge badge-success">News!</span> 10 Tender Terbaru
                </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <?= $this->session->flashdata('message'); ?>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Judul Tender</th>
                            <th>Ruangan</th>
                            <th>Gedung</th>
                            <th>Area</th>
                            <th>Pagu Anggaran</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($v as $info) : ?>
                            <tr>
                                <td><?= $i; ?></td>
                                <td> <a href="<?= base_url('detailInformasi/'); ?><?= $info['reff_tender']; ?>"><?= $info['judul_tender']; ?></a>
                                </td>
                                <td><?= $info['nama_ruangan']; ?></td>
                                <td><?= $info['nama_gedung']; ?></td>
                                <td><?= $info['lokasi']; ?></td>
                                <td><?= number_format($info['pagu_anggaran']); ?></td>
                                <td> <?php
                                        $today = date('Y-m-d');
                                        if ($today >= $info['date_start_tender'] && $today <= $info['date_end_tender']) {
                                            echo '<span class="badge badge-success">OPEN</span> ';
                                        } else {
                                            echo '<span class="badge badge-danger">Close</span>';
                                        }
                                        ?>

                                </td>

                            </tr>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    <span class="badge badge-danger">Selamat!</span> Pemenang Tender
                </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <?= $this->session->flashdata('message'); ?>
                <table id="example2" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Judul Tender</th>
                            <th>Ruangan</th>
                            <th>Gedung</th>
                            <th>Area</th>
                            <th>PIC</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($win as $info) : ?>
                            <tr>
                                <td><?= $i; ?></td>
                                <td><?= $info['judul_tender']; ?></td>
                                <td><?= $info['nama_ruangan']; ?></td>
                                <td><?= $info['nama_gedung']; ?></td>
                                <td><?= $info['lokasi']; ?></td>
                                <td> <?= $info['nama_pt']; ?></td>
                            </tr>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</section>