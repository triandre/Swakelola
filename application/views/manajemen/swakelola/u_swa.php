 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">
                 <h3 class="card-title">
                     Form Jenis Swakelola
                 </h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <form class="form-horizontal" method="post" action="<?= base_url('u_swa_go'); ?>">
                     <div class="card-body">


                         <input hidden type="text" class="form-control" id="id_swakelola" name="id_swakelola"
                             value="<?= $d['id_swakelola'] ?>" readonly required>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Jenis Swakelola</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="jenis_swakelola" name="jenis_swakelola"
                                     value="<?= $d['jenis_swakelola'] ?>" required>
                             </div>
                         </div>


                     </div>
                     <!-- /.card-body -->
                     <div class="card-footer">
                         <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                         <a href="#" onclick="history.back();" class="btn btn-default float-right">Kembali</a>
                     </div>
                     <!-- /.card-footer -->
                 </form>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>
 </section>