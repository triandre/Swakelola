 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">
                 <h3 class="card-title">
                     Form User account
                 </h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <form class="form-horizontal" method="post" action="<?= base_url('c_user'); ?>"
                     enctype="multipart/form-data">
                     <div class="card-body">

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Instansi</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="nama_pt" name="nama_pt" required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">No.Hp (Aktif)</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="no_hp_pt" name="no_hp_pt" required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">No.telp</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="telpon_pt" name="telpon_pt" required>
                             </div>
                         </div>


                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Email (Aktif)</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="email" name="email" required>
                             </div>
                         </div>


                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Nama PIC</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="pic_pt" name="pic_pt" required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Role Akun</label>
                             <div class="col-sm-10">
                                 <select type="text" class="form-control" id="role_id" name="role_id" required>
                                     <option selected disabled value="">Pilih</option>
                                     <option value="1">Pimpinan</option>
                                     <option value="2">Admin</option>
                                     <option value="3">Vendor</option>
                                 </select>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Status</label>
                             <div class="col-sm-10">
                                 <select type="text" class="form-control" id="is_active" name="is_active" required>
                                     <option selected disabled value="">Pilih</option>
                                     <option value="1">Aktif</option>
                                     <option value="0">NonAktif</option>
                                 </select>
                             </div>
                         </div>




                     </div>
                     <!-- /.card-body -->
                     <div class="card-footer">
                         <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                         <a href="#" onclick="history.back();" class="btn btn-default float-right">Kembali</a>
                     </div>
                     <!-- /.card-footer -->
                 </form>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>
 </section>