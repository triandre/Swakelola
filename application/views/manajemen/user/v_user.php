 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">
                 <h3 class="card-title">
                     <a href="<?= base_url('c_user'); ?>" class="btn btn-danger"><i class="fa fa-plus"></i> Tambah
                         Akun</a>
                 </h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <table id="example1" class="table table-bordered table-striped">
                     <thead>
                         <tr>
                             <th>No</th>
                             <th>Email</th>
                             <th>Nama Perusahaan</th>
                             <th>Status</th>
                             <th>keaktifan</th>
                             <th>#</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php $i = 1; ?>
                         <?php foreach ($v as $user) : ?>
                         <tr>
                             <td><?= $i; ?></td>
                             <td><?= $user['email']; ?></td>
                             <td><?= $user['nama_pt']; ?></td>
                             <td>
                                 <?php
                             if ($user['role_id'] == 1) {
                                            echo '<span class="badge badge-success">Pimpinan</span> ';
                                        } elseif ($user['role_id'] == 2) {
                                            echo '<span class="badge badge-warning">Admin</span>';
                                        } elseif ($user['role_id'] == 3) {
                                            echo '<span class="badge badge-danger">Vendor</span>';
                                        }
                                        ?>
                             </td>
                             <td>
                                 <?php
                             if ($user['is_active'] == 1) {
                                            echo '<span class="badge badge-success">Aktif</span> ';
                                        } else {
                                            echo '<span class="badge badge-danger">Nonaktif</span>';
                                      
                                        }
                                        ?></td>
                             <td>
                                 <div class="dropdown">
                                     <button type="button" class="btn btn-primary dropdown-toggle"
                                         data-toggle="dropdown">
                                         <i class="fa fa-cog"></i>
                                     </button>
                                     <div class="dropdown-menu">
                                         <a href="<?= base_url('r_user/'); ?><?= $user['id_akun']; ?>"
                                             class="dropdown-item tombol-reset">Reset Akun</a>
                                         <?php if ($user['is_active'] == 1) { ?>
                                         <a href="<?= base_url('n_user/'); ?><?= $user['id_akun']; ?>"
                                             class="dropdown-item tombol-nonaktif">NonAktif</a>
                                         <?php } else { ?>
                                         <a href="<?= base_url('y_user/'); ?><?= $user['id_akun']; ?>"
                                             class="dropdown-item tombol-aktif">Aktif</a>
                                         <?php } ?>
                                     </div>
                                 </div>
                             </td>

                         </tr>
                         <?php $i++; ?>
                         <?php endforeach; ?>
                     </tbody>

                 </table>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>
 </section>