 <!-- Main content -->

 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">
                 <h3 class="card-title">
                     <a href="<?= base_url('c_kantor'); ?>" class="btn btn-danger"><i class="fa fa-plus"></i> Tambah
                         Data</a>
                 </h3>
             </div>

             <!-- /.card-header -->
             <div class="card-body">
                 <table id="example1" class="table table-bordered table-striped">
                     <thead>
                         <tr>
                             <th>No</th>
                             <th>Nama Kantor</th>
                             <th>Alamat</th>
                             <th>#</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php $i = 1; ?>
                         <?php foreach ($v as $kantor) : ?>
                         <tr>
                             <td><?= $i; ?></td>
                             <td><?= $kantor['lokasi']; ?></td>
                             <td><?= $kantor['alamat']; ?></td>
                             <td>
                                 <div class="dropdown">
                                     <button type="button" class="btn btn-primary dropdown-toggle"
                                         data-toggle="dropdown">
                                         <i class="fa fa-cog"></i>
                                     </button>
                                     <div class="dropdown-menu">
                                         <a href="<?= base_url('u_kantor/'); ?><?= $kantor['id_lokasi']; ?>"
                                             class="dropdown-item">Ubah data</a>
                                         <a href="<?= base_url('h_kantor/'); ?><?= $kantor['id_lokasi']; ?>"
                                             class="dropdown-item tombol-hapus">Hapus Data</a>
                                     </div>
                                 </div>
                             </td>

                         </tr>
                         <?php $i++; ?>
                         <?php endforeach; ?>
                     </tbody>

                 </table>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>
 </section>