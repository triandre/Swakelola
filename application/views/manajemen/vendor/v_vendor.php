 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">

             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <table id="example1" class="table table-bordered table-striped">
                     <thead>
                         <tr>
                             <th>No</th>
                             <th>Nama Perusahaan</th>
                             <th>Nama PIC</th>
                             <th>No. Hp</th>
                             <th>keaktifan</th>
                             <th>#</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php $i = 1; ?>
                         <?php foreach ($v as $user) : ?>
                         <tr>
                             <td><?= $i; ?></td>
                             <td><?= $user['nama_pt']; ?></td>
                             <td><?= $user['pic_pt']; ?></td>
                             <td><?= $user['no_hp_pt']; ?></td>
                             <td>
                                 <?php
                             if ($user['is_active'] == 1) {
                                            echo '<span class="badge badge-success">Aktif</span> ';
                                        } else {
                                            echo '<span class="badge badge-danger">Nonaktif</span>';
                                      
                                        }
                                        ?></td>
                             <td>
                                 <div class="dropdown">
                                     <button type="button" class="btn btn-primary dropdown-toggle"
                                         data-toggle="dropdown">
                                         <i class="fa fa-cog"></i>
                                     </button>
                                     <div class="dropdown-menu">
                                         <a href="<?= base_url('d_user/'); ?><?= $user['id_akun']; ?>"
                                             class="dropdown-item ">Detail PT</a>
                                         <?php if ($user['is_active'] == 1) { ?>
                                         <a href="<?= base_url('n_user/'); ?><?= $user['id_akun']; ?>"
                                             class="dropdown-item tombol-nonaktif">NonAktif</a>
                                         <?php } else { ?>
                                         <a href="<?= base_url('y_user/'); ?><?= $user['id_akun']; ?>"
                                             class="dropdown-item tombol-aktif">Aktif</a>
                                         <?php } ?>
                                     </div>
                                 </div>
                             </td>

                         </tr>
                         <?php $i++; ?>
                         <?php endforeach; ?>
                     </tbody>

                 </table>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>
 </section>