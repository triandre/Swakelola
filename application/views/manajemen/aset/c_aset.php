 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">
                 <h3 class="card-title">
                     Form Aset
                 </h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <form class="form-horizontal" method="post" action="<?= base_url('c_aset'); ?>"
                     enctype="multipart/form-data">
                     <div class="card-body">
                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Nama kantor</label>
                             <div class="col-sm-10">
                                 <select type="text" class="form-control" id="lokasi_id" name="lokasi_id" required>
                                     <option selected disabled value="">Pilih</option>
                                     <?php foreach ($kantor as $row2) : ?>
                                     <option value="<?= $row2['id_lokasi']; ?>"><?= $row2['lokasi']; ?></option>
                                     <?php endforeach ?>
                                 </select>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Gedung</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="nama_gedung" name="nama_gedung" required>
                             </div>
                         </div>


                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Latitude</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="latitude" name="latitude" required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Longitude</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="longitude" name="longitude" required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Image Gedung</label>
                             <div class="col-sm-10">
                                 <input type="file" class="form-control" id="img_gedung" name="img_gedung"
                                     accept="image/png, image/gif, image/jpeg" required>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Status</label>
                             <div class="col-sm-10">
                                 <select type="text" class="form-control" id="active_gedung" name="active_gedung"
                                     required>
                                     <option selected disabled value="">Pilih</option>
                                     <option value="1">Aktif</option>
                                     <option value="0">tidak Aktif</option>
                                 </select>
                             </div>
                         </div>




                     </div>
                     <!-- /.card-body -->
                     <div class="card-footer">
                         <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                         <a href="#" onclick="history.back();" class="btn btn-default float-right">Kembali</a>
                     </div>
                     <!-- /.card-footer -->
                 </form>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>
 </section>