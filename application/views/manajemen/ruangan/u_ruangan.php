 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">
                 <h3 class="card-title">
                     Form Aset
                 </h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <form class="form-horizontal" method="post" action="<?= base_url('u_ruangan_go'); ?>"
                     enctype="multipart/form-data">
                     <div class="card-body">

                         <input type="text" hidden class="form-control" id="id_ruangan" name="id_ruangan"
                             value="<?= $d['id_ruangan']; ?>" readonly required>
                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Gedung</label>
                             <div class="col-sm-10">
                                 <select type="text" class="form-control" id="gedung_id" name="gedung_id" required>
                                     <?php foreach ($v as $key) { ?>
                                     <option value="<?= $key['id_gedung']; ?>"
                                         <?= ($d['gedung_id'] == $key['id_gedung']) ? 'selected' : ''; ?>>
                                         <?= $key['nama_gedung']; ?></option>
                                     <?php } ?>

                                 </select>
                             </div>
                         </div>

                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Lantai</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="lantai" name="lantai"
                                     value="<?= $d['lantai']; ?>" required>
                             </div>
                         </div>


                         <div class="form-group row">
                             <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Ruangan</label>
                             <div class="col-sm-10">
                                 <input type="text" class="form-control" id="nama_ruangan" name="nama_ruangan"
                                     value="<?= $d['nama_ruangan']; ?>" required>
                             </div>
                         </div>


                     </div>
                     <!-- /.card-body -->
                     <div class="card-footer">
                         <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                         <a href="#" onclick="history.back();" class="btn btn-default float-right">Kembali</a>
                     </div>
                     <!-- /.card-footer -->
                 </form>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>
 </section>