 <!-- Main content -->
 <section class="content">
     <div class="container-fluid">
         <div class="card">
             <div class="card-header">
                 <h3 class="card-title">
                     <a href="<?= base_url('c_ruangan'); ?>" class="btn btn-danger"><i class="fa fa-plus"></i> Tambah
                         Data</a>
                 </h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <table id="example1" class="table table-bordered table-striped">
                     <thead>
                         <tr>
                             <th>No</th>
                             <th>Nama Ruangan</th>
                             <th>Gedung</th>
                             <th>Lantai</th>
                             <th>Area</th>
                             <th>#</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php $i = 1; ?>
                         <?php foreach ($v as $aset) : ?>
                         <tr>
                             <td><?= $i; ?></td>
                             <td><?= $aset['nama_ruangan']; ?></td>
                             <td><?= $aset['nama_gedung']; ?></td>
                             <td><?= $aset['lantai']; ?></td>
                             <td><?= $aset['lokasi']; ?></td>
                             <td>
                                 <div class="dropdown">
                                     <button type="button" class="btn btn-primary dropdown-toggle"
                                         data-toggle="dropdown">
                                         <i class="fa fa-cog"></i>
                                     </button>
                                     <div class="dropdown-menu">
                                         <a href="<?= base_url('u_ruangan/'); ?><?= $aset['id_ruangan']; ?>"
                                             class="dropdown-item">Ubah data</a>
                                         <a href="<?= base_url('h_ruangan/'); ?><?= $aset['id_ruangan']; ?>"
                                             class="dropdown-item tombol-hapus">Hapus Data</a>
                                     </div>
                                 </div>
                             </td>

                         </tr>
                         <?php $i++; ?>
                         <?php endforeach; ?>
                     </tbody>

                 </table>
             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>
 </section>