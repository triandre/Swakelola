<?php
defined('BASEPATH') or exit('No direct script access allowed');
$route['default_controller'] = 'Login/index';
$route['translate_uri_dashes'] = FALSE;

//===================================================
//------------------AUTH----------------------------
//===================================================
$route['login'] = 'Login/index';
$route['forgotPassword'] = 'Login/forgotPassword';
$route['login/resetpassword'] = 'Login/resetPassword';
$route['passwordBaru'] = 'Login/changePassword';
$route['login/registration'] = 'Login/registration';
$route['logout'] = 'Login/logout';

//===================================================
//------------------Admin----------------------------
//===================================================
$route['admin'] = 'C_admin/Admin/index';
//===================================================

//===================================================
//------------------PIMPINAN-------------------------
//===================================================
$route['pimpinan'] = 'C_pimpinan/Pimpinan/index';

//===================================================
//------------------Maps-----------------------------
//===================================================
$route['maps'] = 'Maps/index';

//===================================================
//------------------Informasi------------------------
//===================================================
$route['informasi'] = 'C_admin/Informasi/index';
$route['tambahInformasi'] = 'C_admin/Informasi/tambah';
$route['ubahInformasi/(:any)'] = 'C_admin/Informasi/ubah/(:any)';
$route['ubahInformasiGo'] = 'C_admin/Informasi/ubahGo';
$route['hapusInformasi/(:any)'] = 'C_admin/Informasi/hapus/(:any)';
$route['detailInformasi/(:any)'] = 'C_admin/Informasi/detail/(:any)';

//===================================================
//------------------Penawaran------------------------
//===================================================
$route['periksaPenawaran/(:any)'] = 'C_user/Penawaran/periksaPenawaran/(:any)';
$route['buatPenawaran/(:any)'] = 'C_user/Penawaran/tambah/(:any)';
$route['buatPenawaranGo'] = 'C_user/Penawaran/buatPenawaranGo';
$route['penawaran'] = 'C_user/Penawaran/v_penawaran';
$route['ubahPenawaran/(:any)'] = 'C_user/Penawaran/ubahPenawaran/(:any)';
$route['detailPenawaran/(:any)'] = 'C_user/Penawaran/detailPenawaran/(:any)';
$route['cetakPenawaran/(:any)'] = 'C_user/Penawaran/cetakPenawaran/(:any)';

$route['penawaranAdmin'] = 'C_admin/Penawaran/v_penawaran';
$route['lihatPenawaran/(:any)'] = 'C_admin/Penawaran/lihatPenawaran/(:any)';
$route['setujuiPenawaran/(:any)'] = 'C_pimpinan/Penawaran/setujuiPenawaran/(:any)';
$route['setujuiPenawaranGo'] = 'C_pimpinan/Penawaran/setujuiPenawaranGo';
//===================================================
//------------------Pememnang------------------------
//===================================================
$route['pemenang'] = 'Pemenang/v_pemenang';
$route['detailPemenang/(:any)'] = 'Pemenang/detailPemenang/(:any)';

//===================================================
//------------------LAPKER----------------------------
//===================================================
$route['lapker'] = 'Lapker/index';
$route['v_lapker/(:any)/(:any)'] = 'Lapker/v_lapker/(:any)/(:any)';
$route['c_lapker/(:any)/(:any)'] = 'Lapker/c_lapker/(:any)/(:any)';
$route['c_lapkerGo'] = 'Lapker/c_lapkerGo';
$route['u_lapkerGo'] = 'Lapker/u_lapkerGo';
$route['d_lapker/(:any)'] = 'Lapker/d_lapker/(:any)';
$route['u_lapker/(:any)/(:any)/(:any)'] = 'Lapker/u_lapker/(:any)/(:any)/(:any)';


//===================================================
//------------------Tagihan----------------------------
//===================================================
$route['v_tagihan'] = 'Tagihan/v_tagihan';
$route['c_tagihan'] = 'Tagihan/c_tagihan';
$route['c_tagihanGo'] = 'Tagihan/c_tagihanGo';
$route['d_tagihan/(:any)'] = 'Tagihan/d_tagihan/(:any)';
$route['valid_tagihan/(:any)'] = 'Tagihan/valid_tagihan/(:any)';
$route['tolak_tagihan/(:any)'] = 'Tagihan/tolak_tagihan/(:any)';
$route['revisi_tagihan/(:any)'] = 'Tagihan/revisi_tagihan/(:any)';
$route['r_tagihanGo'] = 'Tagihan/r_tagihanGo';
$route['validTolakGo'] = 'Tagihan/validTolakGo';
$route['validTagihanGo'] = 'Tagihan/validTagihanGo';
$route['lapkerAdmin'] = 'Lapker/indexAdmin';
//===================================================
//------------------MANAJEMEN------------------------
//===================================================
//------------------Aset-----------------------------
$route['v_aset'] = 'C_admin/Manajemen/v_aset';
$route['u_aset/(:any)'] = 'C_admin/Manajemen/u_aset/(:any)';
$route['u_aset_go'] = 'C_admin/Manajemen/u_aset_go';
$route['h_aset/(:any)'] = 'C_admin/Manajemen/h_aset/(:any)';
$route['c_aset'] = 'C_admin/Manajemen/c_aset';
//------------------kantor-----------------------------
$route['v_kantor'] = 'C_admin/Manajemen/v_kantor';
$route['u_kantor/(:any)'] = 'C_admin/Manajemen/u_kantor/(:any)';
$route['u_kantor_go'] = 'C_admin/Manajemen/u_kantor_go';
$route['h_kantor/(:any)'] = 'C_admin/Manajemen/h_kantor/(:any)';
$route['c_kantor'] = 'C_admin/Manajemen/c_kantor';
//------------------kantor-----------------------------
$route['v_ruangan'] = 'C_admin/Manajemen/v_ruangan';
$route['u_ruangan/(:any)'] = 'C_admin/Manajemen/u_ruangan/(:any)';
$route['u_ruangan_go'] = 'C_admin/Manajemen/u_ruangan_go';
$route['h_ruangan/(:any)'] = 'C_admin/Manajemen/h_ruangan/(:any)';
$route['c_ruangan'] = 'C_admin/Manajemen/c_ruangan';
//------------------Swakelola-----------------------------
$route['v_swa'] = 'C_admin/Manajemen/v_swa';
$route['u_swa/(:any)'] = 'C_admin/Manajemen/u_swa/(:any)';
$route['u_swa_go'] = 'C_admin/Manajemen/u_swa_go';
$route['h_swa/(:any)'] = 'C_admin/Manajemen/h_swa/(:any)';
$route['c_swa'] = 'C_admin/Manajemen/c_swa';
//------------------User-----------------------------
$route['v_user'] = 'C_admin/User/v_user';
$route['u_user/(:any)'] = 'C_admin/User/u_user/(:any)';
$route['u_user_go'] = 'C_admin/User/u_user_go';
$route['h_user/(:any)'] = 'C_admin/User/h_user/(:any)';
$route['c_user'] = 'C_admin/User/c_user';
$route['n_user/(:any)'] = 'C_admin/User/n_user/(:any)';
$route['y_user/(:any)'] = 'C_admin/User/y_user/(:any)';
$route['r_user/(:any)'] = 'C_admin/User/r_user/(:any)';
//------------------Vendor-----------------------------
$route['v_vendor'] = 'C_admin/Vendor/v_vendor';
$route['n_vendor/(:any)'] = 'C_admin/Vendor/n_vendor/(:any)';
$route['y_vendor/(:any)'] = 'C_admin/Vendor/y_vendor/(:any)';
$route['d_vendor/(:any)'] = 'C_admin/Vendor/d_vendor/(:any)';
//===================================================

//===================================================
//------------------User----------------------------
//===================================================
$route['user'] = 'C_user/User/index';
$route['profil'] = 'C_user/User/profil';
$route['updateProfil'] = 'C_user/User/updateProfil';
$route['updateAkta'] = 'C_user/User/updateAkta';
$route['updateFoto'] = 'C_user/User/updateFoto';
$route['user/akta'] = 'C_user/User/akta';
$route['user/fotoKantor'] = 'C_user/User/fotoKantor';
$route['user/struktur'] = 'C_user/User/struktur';
$route['user/tambahStruktur'] = 'C_user/User/tambahStruktur';
$route['user/ubahStruktur'] = 'C_user/User/ubahStruktur';
$route['user/hapusStruktur/(:any)'] = 'C_user/User/hapusStruktur/(:any)';
$route['detailProfil/(:any)'] = 'C_user/User/detailProfil/(:any)';

//===================================================
//------------------PENGATURAN-----------------------
//===================================================
$route['pengaturanProfil'] = 'Pengaturan/v_profil';
$route['updatePegaturanProfil'] = 'Pengaturan/updatePegaturanProfil';
$route['pengaturanPassword'] = 'Pengaturan/pengaturanPassword';
//===================================================

$route['(:any)'] = 'errors/show_404';
$route['(:any)/(:any)'] = 'errors/show_404';
$route['(:any)/(:any)/(:any)'] = 'errors/show_404';
