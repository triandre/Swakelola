<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_mdl extends CI_Model
{

    public function getUser()
    {
        $this->db->select('*');
        $this->db->from('swa_user');
        $query = $this->db->get();
        return $query->result_array();
    }

	public function detUser()
    {
		$email= $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('swa_user');
		$this->db->where('email',$email );
        $query = $this->db->get();
        return $query->row_array();
    }

  
}