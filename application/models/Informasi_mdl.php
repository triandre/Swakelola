<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Informasi_mdl extends CI_Model
{

    public function getInformasi()
    {
        $this->db->select('*');
        $this->db->from('swa_tender');
        $this->db->join('swa_ruangan', 'swa_ruangan.id_ruangan = swa_tender.ruangan_id', 'left');
        $this->db->join('swa_gedung', 'swa_gedung.id_gedung = swa_ruangan.gedung_id', 'left');
        $this->db->join('swa_lokasi', 'swa_lokasi.id_lokasi = swa_gedung.lokasi_id', 'left');
        $this->db->where('swa_tender.active_tender',1 );
		$this->db->order_by('swa_tender.id_tender', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

	public function getInformasiBeranda()
    {
        $this->db->select('*');
        $this->db->from('swa_tender');
        $this->db->join('swa_ruangan', 'swa_ruangan.id_ruangan = swa_tender.ruangan_id', 'left');
        $this->db->join('swa_gedung', 'swa_gedung.id_gedung = swa_ruangan.gedung_id', 'left');
        $this->db->join('swa_lokasi', 'swa_lokasi.id_lokasi = swa_gedung.lokasi_id', 'left');
        $this->db->where('swa_tender.active_tender',1 );
		$this->db->order_by('swa_tender.id_tender', 'DESC');
		$this->db->limit(10);
        $query = $this->db->get();
        return $query->result_array();
    }

	public function getInformasiTender()
    {
        $this->db->select('*');
        $this->db->from('swa_tender');
        $this->db->join('swa_ruangan', 'swa_ruangan.id_ruangan = swa_tender.ruangan_id', 'left');
        $this->db->join('swa_gedung', 'swa_gedung.id_gedung = swa_ruangan.gedung_id', 'left');
        $this->db->join('swa_lokasi', 'swa_lokasi.id_lokasi = swa_gedung.lokasi_id', 'left');
		$this->db->join('swa_user', 'swa_user.email = swa_tender.win_email', 'left');
		$this->db->where('swa_tender.active_tender',1 );
		$this->db->order_by('swa_tender.id_tender', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    
    public function detInformasi($reff_tender)
    {
        $this->db->select('*');
        $this->db->from('swa_tender');
        $this->db->join('swa_ruangan', 'swa_ruangan.id_ruangan = swa_tender.ruangan_id', 'left');
        $this->db->join('swa_gedung', 'swa_gedung.id_gedung = swa_ruangan.gedung_id', 'left');
        $this->db->join('swa_lokasi', 'swa_lokasi.id_lokasi = swa_gedung.lokasi_id', 'left');
        $this->db->join('swa_swakelola', 'swa_swakelola.id_swakelola = swa_tender.swakelola_id', 'left');
        $this->db->where('swa_tender.reff_tender', $reff_tender);
        $query = $this->db->get();
        return $query->row_array();
    }
}