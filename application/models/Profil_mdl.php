<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profil_mdl extends CI_Model
{

    public function detProfil()
    {
		$email= $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('swa_user');
		$this->db->join('swa_akta', 'swa_akta.email = swa_user.email', 'left');
        $this->db->where('swa_user.email',$email );
        $query = $this->db->get();
        return $query->row_array();
    }

	public function detStruktur()
    {
		$email= $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('swa_struktur');
        $this->db->where('email',$email );
        $query = $this->db->get();
        return $query->result_array();
    }


    
    public function detInformasi($reff_tender)
    {
        $this->db->select('*');
        $this->db->from('swa_tender');
        $this->db->join('swa_ruangan', 'swa_ruangan.id_ruangan = swa_tender.ruangan_id', 'left');
        $this->db->join('swa_gedung', 'swa_gedung.id_gedung = swa_ruangan.gedung_id', 'left');
        $this->db->join('swa_lokasi', 'swa_lokasi.id_lokasi = swa_gedung.lokasi_id', 'left');
        $this->db->join('swa_swakelola', 'swa_swakelola.id_swakelola = swa_tender.swakelola_id', 'left');
        $this->db->where('swa_tender.reff_tender',$reff_tender );
        $query = $this->db->get();
        return $query->row_array();
    }
	public function detProfilInstansi($email)
    {
        $this->db->select('*');
        $this->db->from('swa_user');
		$this->db->join('swa_akta', 'swa_akta.email = swa_user.email', 'left');
        $this->db->where('swa_user.email',$email );
        $query = $this->db->get();
        return $query->row_array();
    }
	public function detStrukturInstansi($email)
    {
		
        $this->db->select('*');
        $this->db->from('swa_struktur');
        $this->db->where('email',$email );
        $query = $this->db->get();
        return $query->result_array();
    }
}
