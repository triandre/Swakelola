<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tagihan_mdl extends CI_Model
{

    public function getTagihan()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('swa_penawaran');
        $this->db->join('swa_tender', 'swa_tender.reff_tender = swa_penawaran.reff_tender', 'left');
        $this->db->join('swa_user', 'swa_user.email = swa_penawaran.email', 'left');
        $this->db->where('swa_penawaran.email', $email);
        $this->db->order_by('swa_penawaran.id_penawaran', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPengerjaan()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('swa_penawaran');
        $this->db->join('swa_tender', 'swa_tender.reff_tender = swa_penawaran.reff_tender', 'left');
        $this->db->join('swa_user', 'swa_user.email = swa_penawaran.email', 'left');
        $this->db->where('swa_penawaran.email', $email);
        $this->db->where('swa_penawaran.sts_penawaran', 2);
        $this->db->order_by('swa_penawaran.id_penawaran', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPenawaran()
    {
        $this->db->select('*');
        $this->db->from('swa_penawaran');
        $this->db->join('swa_tender', 'swa_tender.reff_tender = swa_penawaran.reff_tender', 'left');
        $this->db->join('swa_user', 'swa_user.email = swa_penawaran.email', 'left');
        $this->db->join('swa_ruangan', 'swa_ruangan.id_ruangan = swa_tender.ruangan_id', 'left');
        $this->db->join('swa_gedung', 'swa_gedung.id_gedung = swa_ruangan.gedung_id', 'left');
        $this->db->join('swa_lokasi', 'swa_lokasi.id_lokasi = swa_gedung.lokasi_id', 'left');
        $this->db->join('swa_swakelola', 'swa_swakelola.id_swakelola = swa_tender.swakelola_id', 'left');
        $this->db->where('swa_penawaran.sts_penawaran', 2);
        $this->db->order_by('swa_penawaran.id_penawaran', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAjuan()
    {
        $this->db->select('*');
        $this->db->from('swa_invoice');
        $this->db->join('swa_penawaran', 'swa_penawaran.reff_penawaran = swa_penawaran.reff_penawaran', 'left');
        $this->db->join('swa_tender', 'swa_tender.reff_tender = swa_penawaran.reff_tender', 'left');
        $this->db->join('swa_ruangan', 'swa_ruangan.id_ruangan = swa_tender.ruangan_id', 'left');
        $this->db->join('swa_gedung', 'swa_gedung.id_gedung = swa_ruangan.gedung_id', 'left');
        $this->db->join('swa_lokasi', 'swa_lokasi.id_lokasi = swa_gedung.lokasi_id', 'left');
        $this->db->join('swa_swakelola', 'swa_swakelola.id_swakelola = swa_tender.swakelola_id', 'left');
        $this->db->where('swa_penawaran.sts_penawaran', 2);
        $this->db->order_by('swa_invoice.id_inv', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDetilAjuan($reff_inv)
    {
        $this->db->select('*');
        $this->db->from('swa_invoice');
        $this->db->join('swa_penawaran', 'swa_penawaran.reff_penawaran = swa_invoice.reff_penawaran', 'left');
        $this->db->join('swa_tender', 'swa_tender.reff_tender = swa_penawaran.reff_tender', 'left');
        $this->db->join('swa_user', 'swa_user.email = swa_penawaran.email', 'left');
        $this->db->join('swa_ruangan', 'swa_ruangan.id_ruangan = swa_tender.ruangan_id', 'left');
        $this->db->join('swa_gedung', 'swa_gedung.id_gedung = swa_ruangan.gedung_id', 'left');
        $this->db->join('swa_lokasi', 'swa_lokasi.id_lokasi = swa_gedung.lokasi_id', 'left');
        $this->db->join('swa_swakelola', 'swa_swakelola.id_swakelola = swa_tender.swakelola_id', 'left');
        $this->db->join('swa_pembayaran', 'swa_pembayaran.id_pembayaran = swa_penawaran.pembayaran_id', 'left');
        $this->db->where('swa_invoice.reff_inv', $reff_inv);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function generate_invoice_number()
    {
        // Format nomor invoice, misalnya INV-YYYYMMDD-XXXX (XXXX adalah angka unik)
        $prefix = 'INV-';
        $date = date('Ymd');
        $this->db->select('COUNT(id_inv) as total');
        $this->db->from('swa_invoice');
        $this->db->where('DATE(created_at)', date('Y-m-d'));
        $query = $this->db->get();
        $result = $query->row();
        $count = $result->total + 1;
        $suffix = str_pad($count, 4, '0', STR_PAD_LEFT); // Padding angka dengan nol di depan jika diperlukan
        return $prefix . $date . '-' . $suffix;
    }
}
