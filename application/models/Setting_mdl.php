<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_mdl extends CI_Model
{

    public function list_setting()
    {
        $query = $this->db->get('swa_setting');
        return $query->row();
    }

    public function edit($data)
    {
        $this->db->where('id_setting', $data['id_setting']);
        $this->db->update('swa_setting', $data);
    }
}

/* End of file M_setting.php */
/* Location: ./application/models/M_setting.php */