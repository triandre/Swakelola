<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Manajemen_mdl extends CI_Model
{

    public function getKantor()
    {
        $this->db->select('*');
        $this->db->from('swa_lokasi');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function detKantor($id_lokasi)
    {
        $this->db->select('*');
        $this->db->from('swa_lokasi');
        $this->db->where('id_lokasi', $id_lokasi);
        $query = $this->db->get();
        return $query->row_array();
    }


    public function getAset()
    {
        $this->db->select('*');
        $this->db->from('swa_gedung');
        $this->db->join('swa_lokasi', 'swa_lokasi.id_lokasi = swa_gedung.lokasi_id', 'left');
        $this->db->where('swa_gedung.active_gedung', '1');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function detAset($id_gedung)
    {
        $this->db->select('*');
        $this->db->from('swa_gedung');
        $this->db->where('id_gedung', $id_gedung);
        $query = $this->db->get();
        return $query->row_array();
    }


    public function getSwa()
    {
        $this->db->select('*');
        $this->db->from('swa_swakelola');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function detSwa($id_swakelola)
    {
        $this->db->select('*');
        $this->db->from('swa_swakelola');
        $this->db->where('id_swakelola', $id_swakelola);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRuangan()
    {
        $this->db->select('*');
        $this->db->from('swa_ruangan');
        $this->db->join('swa_gedung', 'swa_gedung.id_gedung = swa_ruangan.gedung_id', 'left');
        $this->db->join('swa_lokasi', 'swa_lokasi.id_lokasi = swa_gedung.lokasi_id', 'left');
        $this->db->where('swa_gedung.active_gedung', '1');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function detRuangan($id_ruangan)
    {
        $this->db->select('*');
        $this->db->from('swa_ruangan');
        $this->db->where('id_ruangan', $id_ruangan);
        $query = $this->db->get();
        return $query->row_array();
    }
}