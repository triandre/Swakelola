<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penawaran_mdl extends CI_Model
{

    public function getPenawaran()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('swa_penawaran');
        $this->db->join('swa_tender', 'swa_tender.reff_tender = swa_penawaran.reff_tender', 'left');
        $this->db->join('swa_user', 'swa_user.email = swa_penawaran.email', 'left');
        $this->db->where('swa_penawaran.email', $email);
        $this->db->order_by('swa_penawaran.id_penawaran', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPenawaranAdmin($reff_tender)
    {
        $this->db->select('*');
        $this->db->from('swa_penawaran');
        $this->db->join('swa_tender', 'swa_tender.reff_tender = swa_penawaran.reff_tender', 'left');
        $this->db->join('swa_user', 'swa_user.email = swa_penawaran.email', 'left');
        $this->db->where('swa_penawaran.reff_tender', $reff_tender);
        $this->db->order_by('swa_penawaran.id_penawaran', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPembayaran()
    {
        $this->db->select('*');
        $this->db->from('swa_pembayaran');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function detPenawaran($reff_penawaran)
    {
        $this->db->select('*');
        $this->db->from('swa_penawaran');
        $this->db->join('swa_tender', 'swa_tender.reff_tender = swa_penawaran.reff_tender', 'left');
        $this->db->join('swa_user', 'swa_user.email = swa_penawaran.email', 'left');
        $this->db->join('swa_ruangan', 'swa_ruangan.id_ruangan = swa_tender.ruangan_id', 'left');
        $this->db->join('swa_gedung', 'swa_gedung.id_gedung = swa_ruangan.gedung_id', 'left');
        $this->db->join('swa_lokasi', 'swa_lokasi.id_lokasi = swa_gedung.lokasi_id', 'left');
        $this->db->join('swa_swakelola', 'swa_swakelola.id_swakelola = swa_tender.swakelola_id', 'left');
        $this->db->join('swa_pembayaran', 'swa_pembayaran.id_pembayaran = swa_penawaran.pembayaran_id', 'left');
        $this->db->where('swa_penawaran.reff_penawaran', $reff_penawaran);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getMenang()
    {
        $this->db->select('*');
        $this->db->from('swa_penawaran');
        $this->db->join('swa_tender', 'swa_tender.reff_tender = swa_penawaran.reff_tender', 'left');
        $this->db->join('swa_user', 'swa_user.email = swa_penawaran.email', 'left');
        $this->db->join('swa_ruangan', 'swa_ruangan.id_ruangan = swa_tender.ruangan_id', 'left');
        $this->db->join('swa_gedung', 'swa_gedung.id_gedung = swa_ruangan.gedung_id', 'left');
        $this->db->join('swa_lokasi', 'swa_lokasi.id_lokasi = swa_gedung.lokasi_id', 'left');
        $this->db->join('swa_swakelola', 'swa_swakelola.id_swakelola = swa_tender.swakelola_id', 'left');
        $this->db->where('swa_penawaran.sts_penawaran', 2);
        $this->db->order_by('swa_penawaran.id_penawaran', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }


    public function detMenang($reff_tender)
    {
        $this->db->select('*');
        $this->db->from('swa_penawaran');
        $this->db->join('swa_tender', 'swa_tender.reff_tender = swa_penawaran.reff_tender', 'left');
        $this->db->join('swa_user', 'swa_user.email = swa_penawaran.email', 'left');
        $this->db->join('swa_ruangan', 'swa_ruangan.id_ruangan = swa_tender.ruangan_id', 'left');
        $this->db->join('swa_gedung', 'swa_gedung.id_gedung = swa_ruangan.gedung_id', 'left');
        $this->db->join('swa_lokasi', 'swa_lokasi.id_lokasi = swa_gedung.lokasi_id', 'left');
        $this->db->join('swa_swakelola', 'swa_swakelola.id_swakelola = swa_tender.swakelola_id', 'left');
        $this->db->join('swa_pembayaran', 'swa_pembayaran.id_pembayaran = swa_penawaran.pembayaran_id', 'left');
        $this->db->where('swa_penawaran.reff_penawaran', $reff_tender);
        $query = $this->db->get();
        return $query->row_array();
    }


    public function getLapker()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('swa_penawaran');
        $this->db->join('swa_tender', 'swa_tender.reff_tender = swa_penawaran.reff_tender', 'left');
        $this->db->join('swa_user', 'swa_user.email = swa_penawaran.email', 'left');
        $this->db->join('swa_ruangan', 'swa_ruangan.id_ruangan = swa_tender.ruangan_id', 'left');
        $this->db->join('swa_gedung', 'swa_gedung.id_gedung = swa_ruangan.gedung_id', 'left');
        $this->db->join('swa_lokasi', 'swa_lokasi.id_lokasi = swa_gedung.lokasi_id', 'left');
        $this->db->join('swa_swakelola', 'swa_swakelola.id_swakelola = swa_tender.swakelola_id', 'left');
        $this->db->where('swa_penawaran.sts_penawaran', 2);
        $this->db->where('swa_penawaran.email', $email);
        $this->db->order_by('swa_penawaran.id_penawaran', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function listLapker($reff_tender)
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('swa_lapker');
        $this->db->join('swa_tender', 'swa_tender.reff_tender = swa_lapker.reff_tender', 'left');
        $this->db->join('swa_ruangan', 'swa_ruangan.id_ruangan = swa_tender.ruangan_id', 'left');
        $this->db->join('swa_gedung', 'swa_gedung.id_gedung = swa_ruangan.gedung_id', 'left');
        $this->db->join('swa_lokasi', 'swa_lokasi.id_lokasi = swa_gedung.lokasi_id', 'left');
        $this->db->join('swa_swakelola', 'swa_swakelola.id_swakelola = swa_tender.swakelola_id', 'left');
        $this->db->where('swa_lapker.reff_tender', $reff_tender);
        $this->db->order_by('swa_lapker.id_lapker', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function detailLapker($reff_tender)
    {
        $this->db->select('*');
        $this->db->from('swa_tender');
        $this->db->where('reff_tender', $reff_tender);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function ubahLapker($reff_lapker)
    {
        $this->db->select('*');
        $this->db->from('swa_lapker');
        $this->db->where('reff_lapker', $reff_lapker);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getLapkerAdmin()
    {
        $this->db->select('*');
        $this->db->from('swa_penawaran');
        $this->db->join('swa_tender', 'swa_tender.reff_tender = swa_penawaran.reff_tender', 'left');
        $this->db->join('swa_user', 'swa_user.email = swa_penawaran.email', 'left');
        $this->db->join('swa_ruangan', 'swa_ruangan.id_ruangan = swa_tender.ruangan_id', 'left');
        $this->db->join('swa_gedung', 'swa_gedung.id_gedung = swa_ruangan.gedung_id', 'left');
        $this->db->join('swa_lokasi', 'swa_lokasi.id_lokasi = swa_gedung.lokasi_id', 'left');
        $this->db->join('swa_swakelola', 'swa_swakelola.id_swakelola = swa_tender.swakelola_id', 'left');
        $this->db->where('swa_penawaran.sts_penawaran', 2);
        $this->db->order_by('swa_penawaran.id_penawaran', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }
}
