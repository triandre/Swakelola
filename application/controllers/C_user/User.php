<?php

defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
	public function __construct()
	{
        parent::__construct();
		is_logged_in();
		$this->load->model('Profil_mdl', 'pm');
	}

	public function index()
	{
		$data = array(
			'title' => 'Swakelola UMSU',
			'active_menu_db' => 'active',
		);
		$this->load->view('layout/header', $data);
		$this->load->view('user/index', $data);
		$this->load->view('layout/footer');
	}

	public function profil()
	{
		$data = array(
			'title' => 'Swakelola UMSU',
			'active_menu_profil' => 'active',
			'p' => $this->pm->detProfil()
		);
		$this->load->view('layout/header', $data);
		$this->load->view('user/profil/v_profil', $data);
		$this->load->view('layout/footer');
	}

	public function struktur()
	{
		$data = array(
			'title' => 'Swakelola UMSU',
			'active_menu_profil' => 'active',
			'p' => $this->pm->detProfil(),
			'vs' => $this->pm->detStruktur()
		);
		$this->load->view('layout/header', $data);
		$this->load->view('user/profil/v_struktur', $data);
		$this->load->view('layout/footer');
	}

	public function akta()
	{
		$data = array(
			'title' => 'Swakelola UMSU',
			'active_menu_profil' => 'active',
			'p' => $this->pm->detProfil()
		);
		$this->load->view('layout/header', $data);
		$this->load->view('user/profil/v_akta', $data);
		$this->load->view('layout/footer');
	}

	public function fotoKantor()
	{
		$data = array(
			'title' => 'Swakelola UMSU',
			'active_menu_profil' => 'active',
			'p' => $this->pm->detProfil()
		);
		$this->load->view('layout/header', $data);
		$this->load->view('user/profil/v_foto_kantor', $data);
		$this->load->view('layout/footer');
	}
	public function updateProfil()
	{
		    $email = htmlspecialchars($this->input->post('email'));
			$nama_pt = htmlspecialchars($this->input->post('nama_pt'));
			$npwp = htmlspecialchars($this->input->post('npwp'));
			$telpon_pt = htmlspecialchars($this->input->post('telpon_pt'));
			$no_hp_pt = htmlspecialchars($this->input->post('no_hp_pt'));
			$bidang_usaha = htmlspecialchars($this->input->post('bidang_usaha'));
			$pic_pt = htmlspecialchars($this->input->post('pic_pt'));
			$bentuk_pt = htmlspecialchars($this->input->post('bentuk_pt'));
			$provinsi = htmlspecialchars($this->input->post('provinsi'));
			$kota = htmlspecialchars($this->input->post('kota'));
			$kode_pos = htmlspecialchars($this->input->post('kode_pos'));
			$alamat = htmlspecialchars($this->input->post('alamat'));
			$deskripsi = htmlspecialchars($this->input->post('deskripsi'));

			$upload_image = $_FILES['image']['name'];
			if ($upload_image) {
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['max_size']      = '10000';
				$config['upload_path'] = './src/img/';
				$config['encrypt_name'] = TRUE;
	
				$this->load->library('upload', $config);
	
				if ($this->upload->do_upload('image')) {
					$upload1 = $this->upload->data('file_name');
					$this->db->set('image', $upload1);
				} else {
					echo $this->upload->display_errors();
				}
			}
            $this->db->set('nama_pt', $nama_pt);
            $this->db->set('npwp', $npwp);
            $this->db->set('telpon_pt', $telpon_pt);
            $this->db->set('no_hp_pt', $no_hp_pt);
            $this->db->set('bidang_usaha', $bidang_usaha);
            $this->db->set('pic_pt', $pic_pt);
            $this->db->set('bentuk_pt', $bentuk_pt);
            $this->db->set('provinsi', $provinsi);
            $this->db->set('kota', $kota);
            $this->db->set('kode_pos', $kode_pos);
            $this->db->set('alamat', $alamat);
            $this->db->set('deskripsi', $deskripsi);
			$this->db->where('email', $email);
			$this->db->update('swa_user');
	 
			
			$log = [
				'log' => "Ubah Profil",
				'email' => $this->session->userdata('email'),
				'date_created' => date('Y-m-d H:i:s')
			];
	
			$this->db->insert('swa_log', $log);
			$this->session->set_flashdata('sukses', 'Disimpan');
			redirect('profil');
		
	}

	public function updateAkta()
	{
		    $email = htmlspecialchars($this->input->post('email'));
			$nama_pt_akta = htmlspecialchars($this->input->post('nama_pt_akta'));
			$no_akta = htmlspecialchars($this->input->post('no_akta'));
			$tgl_akta = htmlspecialchars($this->input->post('tgl_akta'));
			$nama_notaris = htmlspecialchars($this->input->post('nama_notaris'));
			

			$upload_image = $_FILES['file_akta']['name'];
			if ($upload_image) {
				$config['allowed_types'] = 'pdf';
				$config['max_size']      = '10000';
				$config['upload_path'] = './src/archive/';
				$config['encrypt_name'] = TRUE;
	
				$this->load->library('upload', $config);
	
				if ($this->upload->do_upload('file_akta')) {
					$upload1 = $this->upload->data('file_name');
					$this->db->set('file_akta', $upload1);
				} else {
					echo $this->upload->display_errors();
				}
			}
            $this->db->set('nama_pt_akta', $nama_pt_akta);
            $this->db->set('no_akta', $no_akta);
            $this->db->set('tgl_akta', $tgl_akta);
            $this->db->set('nama_notaris', $nama_notaris);
			$this->db->where('email', $email);
			$this->db->update('swa_akta');
	 
			
			$log = [
				'log' => "Ubah Akta",
				'email' => $this->session->userdata('email'),
				'date_created' => date('Y-m-d H:i:s')
			];
	
			$this->db->insert('swa_log', $log);
			$this->session->set_flashdata('sukses', 'Disimpan');
			redirect('user/akta');
		
	}

	public function updateFoto()
	{
		    $email = htmlspecialchars($this->input->post('email'));
			

			$upload_image = $_FILES['foto_kantor']['name'];
			if ($upload_image) {
				$config['allowed_types'] = 'jpg|png|jpeg';
				$config['max_size']      = '10000';
				$config['upload_path'] = './src/img/';
				$config['encrypt_name'] = TRUE;
	
				$this->load->library('upload', $config);
	
				if ($this->upload->do_upload('foto_kantor')) {
					$upload1 = $this->upload->data('file_name');
					$this->db->set('foto_kantor', $upload1);
				} else {
					echo $this->upload->display_errors();
				}
			}
          
			$this->db->where('email', $email);
			$this->db->update('swa_user');
	 
			
			$log = [
				'log' => "Ubah Foto Kantor",
				'email' => $this->session->userdata('email'),
				'date_created' => date('Y-m-d H:i:s')
			];
	
			$this->db->insert('swa_log', $log);
			$this->session->set_flashdata('sukses', 'Disimpan');
			redirect('user/fotoKantor');
		
	}

	public function tambahStruktur()
	{
		
		$data = [
        'email' => $this->session->userdata('email'),
        'nama_pegawai' => htmlspecialchars($this->input->post('nama_pegawai')),
		'jabatan' => htmlspecialchars($this->input->post('jabatan')),
		'no_hp' => htmlspecialchars($this->input->post('no_hp')),
		'email_pegawai' => htmlspecialchars($this->input->post('email_pegawai')),
		'date_created' => date('Y-m-d H:i:s')
		];

		$log = [
			'log' => "Tambah Data Struktur",
			'email' => $this->session->userdata('email'),
			'date_created' => date('Y-m-d H:i:s')
		];

		$this->db->insert('swa_log', $log);
		$this->db->insert('swa_struktur', $data);

		$this->session->set_flashdata('sukses', 'Disimpan');
		redirect('user/struktur');
       
	}

	public function ubahStruktur()
	{
		$id_struktur = htmlspecialchars($this->input->post('id_struktur'));
       $nama_pegawai = htmlspecialchars($this->input->post('nama_pegawai'));
	   $jabatan = htmlspecialchars($this->input->post('jabatan'));
	   $no_hp = htmlspecialchars($this->input->post('no_hp'));
	   $email_pegawai = htmlspecialchars($this->input->post('email_pegawai'));

	   $this->db->set('nama_pegawai', $nama_pegawai);
	   $this->db->set('jabatan', $jabatan);
	   $this->db->set('no_hp', $no_hp);
	   $this->db->set('email_pegawai', $email_pegawai);
	   $this->db->where('id_struktur', $id_struktur);
	   $this->db->update('swa_struktur');

	   $log = [
		'log' => "Ubah Data Struktur ID = $id_struktur",
		'email' => $this->session->userdata('email'),
		'date_created' => date('Y-m-d H:i:s')
	    ];

       $this->db->insert('swa_log', $log);
	   $this->session->set_flashdata('sukses', 'Diubah');
	   redirect('user/struktur');
	}

	public function hapusStruktur()
	{
       $id_struktur = $this->uri->segment(3);
	
	   $this->db->where('id_struktur', $id_struktur);
	   $this->db->delete('swa_struktur');

	   $log = [
		'log' => "Hapus Data Struktur ID = $id_struktur",
		'email' => $this->session->userdata('email'),
		'date_created' => date('Y-m-d H:i:s')
	    ];

       $this->db->insert('swa_log', $log);

	   $this->session->set_flashdata('sukses', 'Dihapus');
	   redirect('user/struktur');
	}


	public function detailProfil()
	{
		$email = $this->uri->segment(2);
		$data = array(
			'title' => 'Swakelola UMSU',
			'active_menu_profil' => 'active',
			'p' => $this->pm->detProfilInstansi($email),
			'vs' => $this->pm->detStrukturInstansi($email)
		);
		$this->load->view('layout/header', $data);
		$this->load->view('user/profil/d_profil', $data);
		$this->load->view('layout/footer');
	}
}