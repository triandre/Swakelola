<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Penawaran extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		is_logged_in();
		$this->load->model('Manajemen_mdl', 'mm');
		$this->load->model('Informasi_mdl', 'im');
		$this->load->model('Penawaran_mdl', 'pm');
		$this->load->library('uuid');
	}

	public function v_penawaran()
	{
		$data = array(
			'title' => 'Data penawaran Harga',
			'active_menu_vpen' => 'active',
			'v' => $this->pm->getPenawaran(),

		);
		$this->load->view('layout/header', $data);
		$this->load->view('penawaran/v_penawaran', $data);
		$this->load->view('layout/footer');
	}

	public function periksaPenawaran()
	{
		$reff_tender = $this->uri->segment(2);
		$email = $this->session->userdata('email');
		//periksa
		$cek = $this->db->query("SELECT * FROM swa_penawaran WHERE email='$email' AND reff_tender='$reff_tender'")->num_rows();
		if ($cek >= '1') {
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>Maaf!</strong> Maaf Anda Sudah Pernah Membuat Penawaran Pada Tender Ini.</div>');
			redirect('informasi');
		} else {
			redirect('buatPenawaran/' . $reff_tender);
		}
	}

	public function tambah()
	{
		$reff_tender = $this->uri->segment(2);
		$data = array(
			'title' => 'Tambah Penawaran Harga',
			'active_menu_vpen' => 'active',
			'v' => $this->mm->getRuangan(),
			'd' => $this->im->detInformasi($reff_tender),
			'swa' => $this->mm->getSwa()
		);

		$this->load->view('layout/header', $data);
		$this->load->view('penawaran/c_penawaran', $data);
		$this->load->view('layout/footer');
	}

	public function buatPenawaranGo()
	{
		$id = $this->uuid->v4();
		$reff_penawaran = str_replace('-', '', $id);
		$email_created = $this->session->userdata('email');
		$upload_image = $_FILES['lampiran_penawaran']['name'];
		if ($upload_image) {
			$config['allowed_types'] = 'pdf|jpg|jpeg|png|doc|docx|xlx|xls';
			$config['max_size']      = '10000';
			$config['upload_path'] = './src/archive/';
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);

			if ($this->upload->do_upload('lampiran_penawaran')) {
				$upload1 = $this->upload->data('file_name');
			} else {
				echo $this->upload->display_errors();
			}
		}
		$data = [
			'reff_penawaran' => $reff_penawaran,
			'reff_tender' => htmlspecialchars($this->input->post('reff_tender')),
			'anggaran_penawaran' => preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('anggaran_penawaran', true))),
			'deskripsi_penawaran' => htmlspecialchars($this->input->post('deskripsi_penawaran')),
			'email' => $email_created,
			'nama_pic' => htmlspecialchars($this->input->post('nama_pic')),
			'no_pic' => htmlspecialchars($this->input->post('no_pic')),
			'date_start_penawaran' => htmlspecialchars($this->input->post('date_start_penawaran')),
			'date_end_penawaran' => htmlspecialchars($this->input->post('date_end_penawaran')),
			'sts_penawaran' => 1,
			'lampiran_penawaran' => $upload1,
			'date_created' => date('Y-m-d H:i:s')
		];

		$log = [
			'log' => "Tambah Penawaran = $reff_penawaran",
			'email' => $this->session->userdata('email'),
			'date_created' => date('Y-m-d H:i:s')
		];

		$this->db->insert('swa_log', $log);
		$this->db->insert('swa_penawaran', $data);

		$this->session->set_flashdata('sukses', 'Disimpan');
		redirect('penawaran');
	}

	public function detailPenawaran()
	{
		$reff_penawaran = $this->uri->segment(2);
		$data = array(
			'title' => 'Data Penawaran Harga',
			'active_menu_vpen' => 'active',
			'd' => $this->pm->detPenawaran($reff_penawaran)
		);

		$this->load->view('layout/header', $data);
		$this->load->view('penawaran/d_penawaran', $data);
		$this->load->view('layout/footer');
	}


	public function cetakPenawaran()
	{
		$reff_penawaran = $this->uri->segment(2);
		$data = array(
			'title' => 'Data Penawaran Harga',
			'active_menu_vpen' => 'active',
			'd' => $this->pm->detPenawaran($reff_penawaran)
		);
		$this->load->helper("rpkata");
		$this->load->view('penawaran/p_penawaran', $data);
	}


	public function ubah()
	{
		$reff_tender = $this->uri->segment(2);
		$data = array(
			'title' => 'data Penawaran Harga',
			'active_menu_vinfo' => 'active',
			'd' => $this->im->detInformasi($reff_tender),
			'v' => $this->mm->getRuangan(),
			'swa' => $this->mm->getSwa()
		);

		$this->load->view('layout/header', $data);
		$this->load->view('informasi/u_info', $data);
		$this->load->view('layout/footer');
	}


	public function ubahGo()
	{

		$reff_tender = htmlspecialchars($this->input->post('reff_tender'));
		$ruangan_id = htmlspecialchars($this->input->post('ruangan_id'));
		$swakelola_id = htmlspecialchars($this->input->post('swakelola_id'));
		$judul_tender = htmlspecialchars($this->input->post('judul_tender'));
		$keterangan = htmlspecialchars($this->input->post('keterangan'));
		$pagu_anggaran = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('pagu_anggaran', true)));
		$date_end_work = htmlspecialchars($this->input->post('date_end_work'));
		$date_start_tender = htmlspecialchars($this->input->post('date_start_tender'));
		$date_end_tender = htmlspecialchars($this->input->post('date_end_tender'));

		$upload_image = $_FILES['lampiran']['name'];
		if ($upload_image) {
			$config['allowed_types'] = 'pdf';
			$config['max_size']      = '10000';
			$config['upload_path'] = './src/archive/';
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);

			if ($this->upload->do_upload('lampiran')) {
				$upload1 = $this->upload->data('file_name');
				$this->db->set('lampiran', $upload1);
			} else {
				echo $this->upload->display_errors();
			}
		}


		$this->db->set('ruangan_id', $ruangan_id);
		$this->db->set('swakelola_id', $swakelola_id);
		$this->db->set('judul_tender', $judul_tender);
		$this->db->set('keterangan', $keterangan);
		$this->db->set('pagu_anggaran', $pagu_anggaran);
		$this->db->set('date_end_work', $date_end_work);
		$this->db->set('date_start_tender', $date_start_tender);
		$this->db->set('date_end_tender', $date_end_tender);
		$this->db->where('reff_tender', $reff_tender);
		$this->db->update('swa_tender');


		$log = [
			'log' => "Tambah Data Informasi REFF ID = $reff_tender",
			'email' => $this->session->userdata('email'),
			'date_created' => date('Y-m-d H:i:s')
		];

		$this->db->insert('swa_log', $log);

		$this->session->set_flashdata('sukses', 'Disimpan');
		redirect('informasi');
	}

	public function hapus()
	{

		$reff_tender = $this->uri->segment(2);
		$active_tender = 0;

		$this->db->set('active_tender', $active_tender);
		$this->db->where('reff_tender', $reff_tender);
		$this->db->update('swa_gedung');

		$log = [
			'log' => "Hapus Data Tender = $reff_tender",
			'email' => $this->session->userdata('email'),
			'date_created' => date('Y-m-d H:i:s')
		];

		$this->db->insert('swa_log', $log);

		$this->session->set_flashdata('sukses', 'Disimpan');
		redirect('informasi');
	}
}
