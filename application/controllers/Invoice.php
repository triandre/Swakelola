<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Invoice extends CI_Controller
{
	public function __construct()
	{
        parent::__construct();
		is_logged_in();
		$this->load->model('Manajemen_mdl', 'mm');
		$this->load->model('Informasi_mdl', 'im');
		$this->load->model('Penawaran_mdl', 'pm');
		$this->load->library('uuid');
	}

	public function v_pemenang()
	{
		$data = array(
			'title' => 'Data Pemenang Tender',
			'active_menu_vmenang' => 'active',
			'v' => $this->pm->getMenang(),
            
		);
		$this->load->view('layout/header', $data);
		$this->load->view('pemenang/v_pemenang', $data);
		$this->load->view('layout/footer');
	}

	public function detailPemenang()
	{
		$reff_penawaran = $this->uri->segment(2);
		$data = array(
			'title' => 'Detail Data',
			'active_menu_vmenang' => 'active',
            'd' => $this->pm->detMenang($reff_penawaran)
		);

		$this->load->view('layout/header', $data);
		$this->load->view('penawaran/d_penawaran', $data);
		$this->load->view('layout/footer');
	}

	
}