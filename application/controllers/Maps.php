<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Maps extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        //Do your magic here
        $this->load->library(array("googlemaps"));
        $this->load->model('Manajemen_mdl', 'mm');
        $this->load->model('Setting_mdl', 'mset');
    }


    public function index()
    {
        $setting = $this->mset->list_setting();
        //tampilan awal view map
        $this->load->library("googlemaps");
        $config['center'] = "$setting->latitude,$setting->longitude";
        $config['zoom'] = "$setting->zoom";
        $this->googlemaps->initialize($config);
        $nasabah = $this->mm->getAset();
        foreach ($nasabah as $key => $value) : //perulangan data
            $marker = array();
            $marker['position'] = $value['latitude'] . ',' . $value['longitude'];
            $marker['infowindow_content'] = '<div class="media" style="width:400px;">';
            $marker['infowindow_content'] .= '<div class="media-left">';
            $marker['infowindow_content'] .= '<img src="' . base_url("src/img/{$value['img_gedung']}") . '" class="media-object" style="width:150px">';
            $marker['infowindow_content'] .= '</div>';
            $marker['infowindow_content'] .= '<div class="media-body">';
            $marker['infowindow_content'] .= '<h4 class="media-heading">' . $value['nama_gedung'] . '</h4>';
            $marker['infowindow_content'] .= '<p>Lokasi : ' . $value['lokasi'] . '</p>';
            $marker['infowindow_content'] .= '<p>Alamat : ' . $value['alamat'] . '</p>';
            $marker['infowindow_content'] .= '</div>';
            $marker['infowindow_content'] .= '</div>';
            $marker['icon'] = base_url('src/icon/villa.png');
            $this->googlemaps->add_marker($marker);
        endforeach;
        //end perulangan data
        //tampilan data marker map
        $this->googlemaps->initialize($config);
        $map = $this->googlemaps->create_map();
        //menampilkan maker ke map

        $data = array(
            'title' => 'Maping Aset',
            'active_menu_maps' => 'active',
            'map' => $map,

        );

        $this->load->view('layout/header', $data);
        $this->load->view('maps/v_map', $data);
        $this->load->view('layout/footer');
    }
}

/* End of file Admin.php */