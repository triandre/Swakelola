<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Pengaturan extends CI_Controller
{
	public function __construct()
	{
        parent::__construct();
		is_logged_in();
		$this->load->model('User_mdl', 'mm');
		$this->load->library('uuid');
	}

	public function v_profil()
	{
		$data = array(
			'title' => 'Pengaturan Profil',
			'active_menu_pengaturan' => 'menu-open',
            'active_menu_prt' => 'active',
			'active_menu_vpp' => 'active',
			'p' => $this->mm->detUser(),
            
		);
		$this->load->view('layout/header', $data);
		$this->load->view('pengaturan/v_user', $data);
		$this->load->view('layout/footer');
	}

	public function updatePegaturanProfil()
	{
		    $email = htmlspecialchars($this->input->post('email'));
			$nama_pt = htmlspecialchars($this->input->post('nama_pt'));
			$npwp = htmlspecialchars($this->input->post('npwp'));
			$telpon_pt = htmlspecialchars($this->input->post('telpon_pt'));
			$no_hp_pt = htmlspecialchars($this->input->post('no_hp_pt'));
			$bidang_usaha = htmlspecialchars($this->input->post('bidang_usaha'));
			$pic_pt = htmlspecialchars($this->input->post('pic_pt'));
			$bentuk_pt = htmlspecialchars($this->input->post('bentuk_pt'));
			$provinsi = htmlspecialchars($this->input->post('provinsi'));
			$kota = htmlspecialchars($this->input->post('kota'));
			$kode_pos = htmlspecialchars($this->input->post('kode_pos'));
			$alamat = htmlspecialchars($this->input->post('alamat'));
			$deskripsi = htmlspecialchars($this->input->post('deskripsi'));

			$upload_image = $_FILES['image']['name'];
			if ($upload_image) {
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['max_size']      = '10000';
				$config['upload_path'] = './src/img/';
				$config['encrypt_name'] = TRUE;
	
				$this->load->library('upload', $config);
	
				if ($this->upload->do_upload('image')) {
					$upload1 = $this->upload->data('file_name');
					$this->db->set('image', $upload1);
				} else {
					echo $this->upload->display_errors();
				}
			}
            $this->db->set('nama_pt', $nama_pt);
            $this->db->set('npwp', $npwp);
            $this->db->set('telpon_pt', $telpon_pt);
            $this->db->set('no_hp_pt', $no_hp_pt);
            $this->db->set('bidang_usaha', $bidang_usaha);
            $this->db->set('pic_pt', $pic_pt);
            $this->db->set('bentuk_pt', $bentuk_pt);
            $this->db->set('provinsi', $provinsi);
            $this->db->set('kota', $kota);
            $this->db->set('kode_pos', $kode_pos);
            $this->db->set('alamat', $alamat);
            $this->db->set('deskripsi', $deskripsi);
			$this->db->where('email', $email);
			$this->db->update('swa_user');
	 
			
			$log = [
				'log' => "Ubah Profil",
				'email' => $this->session->userdata('email'),
				'date_created' => date('Y-m-d H:i:s')
			];
	
			$this->db->insert('swa_log', $log);
			$this->session->set_flashdata('sukses', 'Disimpan');
			redirect('pengaturanProfil');
		
	}

	public function pengaturanPassword()
	{
		$data = array(
			'title' => 'Pengaturan Ubah Password',
			'active_menu_pengaturan' => 'menu-open',
            'active_menu_prt' => 'active',
			'active_menu_ppswd' => 'active',
			'p' => $this->mm->detUser()
            
		);
		$data2['user'] = $this->db->get_where('swa_user', ['email' => $this->session->userdata('email')])->row_array();

		$this->form_validation->set_rules('current_password', 'Current Password', 'required|trim');
		$this->form_validation->set_rules('new_password1', 'New Password', 'required|trim|min_length[6]|matches[new_password2]');
		$this->form_validation->set_rules('new_password2', 'Confirm New Password', 'required|trim|min_length[6]|matches[new_password1]');

		if ($this->form_validation->run() == false) {
			$this->load->view('layout/header', $data);
			$this->load->view('pengaturan/v_password', $data);
			$this->load->view('layout/footer');
		} else {
			$current_password = $this->input->post('current_password');
			$new_password = $this->input->post('new_password1');
			if (!password_verify($current_password, $data2['user']['password'])) {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               Kata Sandi Salah Saat ini ! </div>');
			   redirect('pengaturanPassword');
			} else {
				if ($current_password == $new_password) {
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Kata sandi baru tidak boleh sama dengan kata sandi saat ini! </div>');
					redirect('pengaturanPassword');
				} else {
					// password sudah ok
					$password_hash = password_hash($new_password, PASSWORD_DEFAULT);
					$email= $this->session->userdata('email');
					
					$this->db->set('password', $password_hash);
					$this->db->where('email', $email);
					$this->db->update('swa_user');

					$log = [
						'log' => "Ubah Password",
						'email' => $this->session->userdata('email'),
						'date_created' => date('Y-m-d H:i:s')
					];
			
					$this->db->insert('swa_log', $log);
					$this->session->set_flashdata('sukses', 'Disimpan');
					
					redirect('pengaturanPassword');
				}
			}
		}
		
	}
	
}