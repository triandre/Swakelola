<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Tagihan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Manajemen_mdl', 'mm');
        $this->load->model('Informasi_mdl', 'im');
        $this->load->model('Penawaran_mdl', 'pm');
        $this->load->model('Tagihan_mdl', 'tm');
        $this->load->library('uuid');
    }

    public function c_tagihan()
    {
        $data = array(
            'title' => 'Permohonan Tagihan',
            'active_menu_tagihan' => 'menu-open',
            'active_menu_tgh' => 'active',
            'active_menu_ctg' => 'active',
            'tender' => $this->tm->getPengerjaan()

        );
        $this->load->view('layout/header', $data);
        $this->load->view('tagihan/c_tagihan', $data);
        $this->load->view('layout/footer');
    }

    public function c_tagihanGo()
    {
        $id = $this->uuid->v4();
        $reff_inv = str_replace('-', '', $id);
        $invoice_number = $this->tm->generate_invoice_number();
        $upload_image = $_FILES['lampiran_invoice']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf|jpg|jpeg|png';
            $config['max_size']      = '10000';
            $config['upload_path'] = './src/archive/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('lampiran_invoice')) {
                $upload1 = $this->upload->data('file_name');
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = [
            'reff_inv' => $reff_inv,
            'reff_penawaran' => htmlspecialchars($this->input->post('reff_penawaran')),
            'no_inv' => $invoice_number,
            'catatan_pemohon' => htmlspecialchars($this->input->post('catatan_pemohon')),
            'lampiran_invoice' => $upload1,
            'active_inv' => 1,
            'email_pt' => $this->session->userdata('email'),
            'sts_invoice' => 1,
            'date_created' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d')
        ];

        $log = [
            'log' => "Tambah Penawaran $invoice_number",
            'email' => $this->session->userdata('email'),
            'date_created' => date('Y-m-d H:i:s')
        ];

        $this->db->insert('swa_log', $log);
        $this->db->insert('swa_invoice', $data);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('v_tagihan');
    }

    public function v_tagihan()
    {
        $data = array(
            'title' => 'Permohonan Tagihan',
            'active_menu_tagihan' => 'menu-open',
            'active_menu_tgh' => 'active',
            'active_menu_vtg' => 'active',
            'v' => $this->tm->getAjuan()
        );

        $this->load->view('layout/header', $data);
        $this->load->view('tagihan/v_tagihan', $data);
        $this->load->view('layout/footer');
    }

    public function d_tagihan()
    {

        $reff_inv = $this->uri->segment(2);
        $data = array(
            'title' => 'Permohonan Tagihan',
            'active_menu_tagihan' => 'menu-open',
            'active_menu_tgh' => 'active',
            'active_menu_vtg' => 'active',
            'd' => $this->tm->getDetilAjuan($reff_inv)
        );

        $this->load->view('layout/header', $data);
        $this->load->view('tagihan/d_tagihan', $data);
        $this->load->view('layout/footer');
    }

    public function valid_tagihan()
    {

        $reff_inv = $this->uri->segment(2);
        $data = array(
            'title' => 'Permohonan Tagihan',
            'active_menu_tagihan' => 'menu-open',
            'active_menu_tgh' => 'active',
            'active_menu_vtg' => 'active',
            'd' => $this->tm->getDetilAjuan($reff_inv)
        );

        $this->load->view('layout/header', $data);
        $this->load->view('tagihan/valid_tagihan', $data);
        $this->load->view('layout/footer');
    }

    public function validTagihanGo()
    {

        $reff_inv = htmlspecialchars($this->input->post('reff_inv'));
        $jenis_pembayaran = htmlspecialchars($this->input->post('jenis_pembayaran'));
        $tahapan = htmlspecialchars($this->input->post('tahapan'));
        $persen = htmlspecialchars($this->input->post('persen'));
        $nominal = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('nominal', true)));
        $sts_invoice = 2;
        $ket_inv = htmlspecialchars($this->input->post('ket_inv'));
        $date_valid = date('Y-m-d H:i:s');

        $this->db->set('jenis_pembayaran', $jenis_pembayaran);
        $this->db->set('tahapan', $tahapan);
        $this->db->set('tahapan', $tahapan);
        $this->db->set('persen', $persen);
        $this->db->set('nominal', $nominal);
        $this->db->set('ket_inv', $ket_inv);
        $this->db->set('sts_invoice', $sts_invoice);
        $this->db->set('date_valid', $date_valid);
        $this->db->where('reff_inv', $reff_inv);
        $this->db->update('swa_invoice');

        $log = [
            'log' => "Validasi Tagihan = $reff_inv",
            'email' => $this->session->userdata('email'),
            'date_created' => date('Y-m-d H:i:s')
        ];

        $this->db->insert('swa_log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('v_tagihan');
    }

    public function tolak_tagihan()
    {

        $reff_inv = $this->uri->segment(2);
        $data = array(
            'title' => 'Permohonan Tagihan',
            'active_menu_tagihan' => 'menu-open',
            'active_menu_tgh' => 'active',
            'active_menu_vtg' => 'active',
            'd' => $this->tm->getDetilAjuan($reff_inv)
        );

        $this->load->view('layout/header', $data);
        $this->load->view('tagihan/tolak_tagihan', $data);
        $this->load->view('layout/footer');
    }

    public function validTolakGo()
    {

        $reff_inv = htmlspecialchars($this->input->post('reff_inv'));
        $sts_invoice = 5;
        $ket_inv = htmlspecialchars($this->input->post('ket_inv'));
        $date_valid = date('Y-m-d H:i:s');

        $this->db->set('ket_inv', $ket_inv);
        $this->db->set('sts_invoice', $sts_invoice);
        $this->db->set('date_valid', $date_valid);
        $this->db->where('reff_inv', $reff_inv);
        $this->db->update('swa_invoice');

        $log = [
            'log' => "Menolak Permohonan Tagihan = $reff_inv",
            'email' => $this->session->userdata('email'),
            'date_created' => date('Y-m-d H:i:s')
        ];

        $this->db->insert('swa_log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('v_tagihan');
    }


    public function revisi_tagihan()
    {

        $reff_inv = $this->uri->segment(2);
        $data = array(
            'title' => 'Perbaiki Permohonan Tagihan',
            'active_menu_tagihan' => 'menu-open',
            'active_menu_tgh' => 'active',
            'active_menu_vtg' => 'active',
            'd' => $this->tm->getDetilAjuan($reff_inv),
            'tender' => $this->tm->getPengerjaan()
        );

        $this->load->view('layout/header', $data);
        $this->load->view('tagihan/revisi_tagihan', $data);
        $this->load->view('layout/footer');
    }

    public function r_tagihanGo()
    {
        $reff_inv = htmlspecialchars($this->input->post('reff_inv'));
        $catatan_pemohon = htmlspecialchars($this->input->post('catatan_pemohon'));
        $sts_invoice = 1;
        $upload_image = $_FILES['lampiran_invoice']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf|jpg|jpeg|png';
            $config['max_size']      = '10000';
            $config['upload_path'] = './src/archive/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('lampiran_invoice')) {
                $upload1 = $this->upload->data('file_name');
                $this->db->set('lampiran_invoice', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }


        $this->db->set('catatan_pemohon', $catatan_pemohon);
        $this->db->set('sts_invoice', $sts_invoice);
        $this->db->where('reff_inv', $reff_inv);
        $this->db->update('swa_invoice');



        $log = [
            'log' => "Perbaiki Permohonan Tagihan $reff_inv",
            'email' => $this->session->userdata('email'),
            'date_created' => date('Y-m-d H:i:s')
        ];

        $this->db->insert('swa_log', $log);


        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('v_tagihan');
    }
}
