<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Vendor extends CI_Controller
{
	public function __construct()
	{
        parent::__construct();
		is_logged_in();
		$this->load->model('User_mdl', 'um');
		$this->load->library('uuid');
	}

	public function v_vendor()
	{
		$data = array(
			'title' => 'Vendor',
			'active_menu_vendor' => 'active',
			'v' => $this->um->getUser(),
            
		);
		$this->load->view('layout/header', $data);
		$this->load->view('manajemen/vendor/v_vendor', $data);
		$this->load->view('layout/footer');
	}


    public function c_vendor()
	{
		$data = array(
			'title' => 'Vendor',
			'active_menu_vendor' => 'active',
			'v' => $this->um->getUser()

		);
		$this->form_validation->set_rules('nama_pt', 'Nama Instansi', 'required');
		$this->form_validation->set_rules('no_hp_pt', 'No Hp', 'required');
		$this->form_validation->set_rules('telpon_pt', 'No Telpon', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('pic_pt', 'Nama PIC ', 'required');

		if ($this->form_validation->run() ==  false) {
            $this->load->view('layout/header', $data);
            $this->load->view('manajemen/vendor/c_vendor', $data);
            $this->load->view('layout/footer');
		} else {
			$email = htmlspecialchars($this->input->post('email'));
            
			$data=[
				'email' => htmlspecialchars($this->input->post('email')),
				'nama_pt' => htmlspecialchars($this->input->post('nama_pt')),
				'role_id' => htmlspecialchars($this->input->post('role_id')),
				'is_active' => htmlspecialchars($this->input->post('is_active')),
				'telpon_pt' => htmlspecialchars($this->input->post('telpon_pt')),
				'no_hp_pt' => htmlspecialchars($this->input->post('no_hp_pt')),
                'password' => password_hash("#unggulcerdas", PASSWORD_DEFAULT),
				'pic_pt' => htmlspecialchars($this->input->post('pic_pt')),
                'image' => "default.jpg",
				'date_created' => date('Y-m-d H:i:s')
			];

			$log = [
				'log' => "Tambah Data User Email = $reff_tender",
				'email' => $this->session->userdata('email'),
				'date_created' => date('Y-m-d H:i:s')
			];
	
			$this->db->insert('swa_log', $log);
			$this->db->insert('swa_vendor', $data);
	
			$this->session->set_flashdata('sukses', 'Disimpan');
			redirect('v_vendor');
		}
    }

    public function n_vendor()
	{
		
		$id_akun = $this->uri->segment(2);
		$is_active = 0;
				
        $this->db->set('is_active', $is_active);
        $this->db->where('id_akun', $id_akun);
        $this->db->update('swa_vendor');

			$log = [
				'log' => "NonAktif User ID = $id_akun",
				'email' => $this->session->userdata('email'),
				'date_created' => date('Y-m-d H:i:s')
			];
	
			$this->db->insert('swa_log', $log);
	
			$this->session->set_flashdata('sukses', 'Menonaktifkan Akun');
			redirect('v_vendor');
		
	}

    public function y_vendor()
	{
		
		$id_akun = $this->uri->segment(2);
		$is_active = 1;
				
        $this->db->set('is_active', $is_active);
        $this->db->where('id_akun', $id_akun);
        $this->db->update('swa_vendor');

			$log = [
				'log' => "Aktif User ID = $id_akun",
				'email' => $this->session->userdata('email'),
				'date_created' => date('Y-m-d H:i:s')
			];
	
			$this->db->insert('swa_log', $log);
	
			$this->session->set_flashdata('sukses', 'Mengaktifkan Akun');
			redirect('v_vendor');
		
	}

    public function r_vendor()
	{
		
		$id_akun = $this->uri->segment(2);
        $password = password_hash("#unggulcerdas", PASSWORD_DEFAULT);
				
        $this->db->set('password', $password);
        $this->db->where('id_akun', $id_akun);
        $this->db->update('swa_vendor');

			$log = [
				'log' => "Aktif User ID = $id_akun",
				'email' => $this->session->userdata('email'),
				'date_created' => date('Y-m-d H:i:s')
			];
	
			$this->db->insert('swa_log', $log);
	
			$this->session->set_flashdata('sukses', 'Reset Password');
			redirect('v_vendor');
		
	}
	
}