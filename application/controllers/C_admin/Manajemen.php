<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Manajemen extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		is_logged_in();
		$this->load->model('Manajemen_mdl', 'mm');
	}


	public function v_kantor()
	{
		$data = array(
			'title' => 'Manajemen kantor',
			'active_menu_bar' => 'menu-open',
			'active_menu_br' => 'active',
			'active_menu_vkantor' => 'active',
			'v' => $this->mm->getKantor()
		);
		$this->load->view('layout/header', $data);
		$this->load->view('manajemen/kantor/v_kantor', $data);
		$this->load->view('layout/footer');
	}


	public function c_kantor()
	{
		$data = array(
			'title' => 'Manajemen kantor',
			'active_menu_bar' => 'menu-open',
			'active_menu_br' => 'active',
			'active_menu_vkantor' => 'active',
			'v' => $this->mm->getKantor()
		);
		$this->form_validation->set_rules('lokasi', 'Nama Kantor', 'required');
		$this->form_validation->set_rules('alamat', 'alamat', 'required');

		if ($this->form_validation->run() ==  false) {
			$this->load->view('layout/header', $data);
			$this->load->view('manajemen/kantor/c_kantor', $data);
			$this->load->view('layout/footer');
		} else {
			$ket = htmlspecialchars($this->input->post('lokasi'));
			$data = [
				'lokasi' => htmlspecialchars($this->input->post('lokasi')),
				'alamat' => htmlspecialchars($this->input->post('alamat')),
				'date_created' => date('Y-m-d H:i:s')
			];

			$log = [
				'log' => "Tambah Data Lokasi $ket",
				'email' => $this->session->userdata('email'),
				'date_created' => date('Y-m-d H:i:s')
			];

			$this->db->insert('swa_log', $log);
			$this->db->insert('swa_lokasi', $data);

			$this->session->set_flashdata('sukses', 'Disimpan');
			redirect('v_kantor');
		}
	}


	public function u_kantor()
	{
		$id_lokasi = $this->uri->segment(2);
		$data = array(
			'title' => 'Manajemen kantor',
			'active_menu_bar' => 'menu-open',
			'active_menu_br' => 'active',
			'active_menu_vkantor' => 'active',
			'd' => $this->mm->detKantor($id_lokasi)
		);

		$this->load->view('layout/header', $data);
		$this->load->view('manajemen/kantor/u_kantor', $data);
		$this->load->view('layout/footer');
	}


	public function u_kantor_go()
	{

		$id_lokasi = htmlspecialchars($this->input->post('id_lokasi'));
		$lokasi = htmlspecialchars($this->input->post('lokasi'));
		$alamat = htmlspecialchars($this->input->post('alamat'));


		$this->db->set('lokasi', $lokasi);
		$this->db->set('alamat', $alamat);
		$this->db->where('id_lokasi', $id_lokasi);
		$this->db->update('swa_lokasi');

		$log = [
			'log' => "Ubah Data Lokasi ID = $id_lokasi",
			'email' => $this->session->userdata('email'),
			'date_created' => date('Y-m-d H:i:s')
		];

		$this->db->insert('swa_log', $log);
		$this->session->set_flashdata('sukses', 'Diubah');
		redirect('v_kantor');
	}

	public function h_kantor()
	{
		$id_lokasi = $this->uri->segment(2);

		$this->db->where('id_lokasi', $id_lokasi);
		$this->db->delete('swa_lokasi');

		$log = [
			'log' => "Hapus Data Lokasi ID = $id_lokasi",
			'email' => $this->session->userdata('email'),
			'date_created' => date('Y-m-d H:i:s')
		];

		$this->db->insert('swa_log', $log);

		$this->session->set_flashdata('sukses', 'Dihapus');
		redirect('v_kantor');
	}


	public function v_aset()
	{
		$data = array(
			'title' => 'Manajemen Aset',
			'active_menu_bar' => 'menu-open',
			'active_menu_br' => 'active',
			'active_menu_vaset' => 'active',
			'v' => $this->mm->getAset()
		);
		$this->load->view('layout/header', $data);
		$this->load->view('manajemen/aset/v_aset', $data);
		$this->load->view('layout/footer');
	}

	public function c_aset()
	{
		$data = array(
			'title' => 'Manajemen Aset',
			'active_menu_bar' => 'menu-open',
			'active_menu_br' => 'active',
			'active_menu_vaset' => 'active',
			'kantor' => $this->mm->getKantor()
		);
		$this->form_validation->set_rules('lokasi_id', 'Nama Kantor', 'required');
		$this->form_validation->set_rules('nama_gedung', 'Nama Gedung', 'required');
		$this->form_validation->set_rules('latitude', 'latitude', 'required');
		$this->form_validation->set_rules('longitude', 'longitude', 'required');

		if ($this->form_validation->run() ==  false) {
			$this->load->view('layout/header', $data);
			$this->load->view('manajemen/aset/c_aset', $data);
			$this->load->view('layout/footer');
		} else {
			$nama_gedung = htmlspecialchars($this->input->post('nama_gedung'));
			$upload_image = $_FILES['img_gedung']['name'];
			if ($upload_image) {
				$config['allowed_types'] = 'jpg|jpeg|png';
				$config['max_size']      = '10000';
				$config['upload_path'] = './src/img/';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);

				if ($this->upload->do_upload('img_gedung')) {
					$upload1 = $this->upload->data('file_name');
				} else {
					echo $this->upload->display_errors();
				}
			}
			$data = [
				'lokasi_id' => htmlspecialchars($this->input->post('lokasi_id')),
				'nama_gedung' => htmlspecialchars($this->input->post('nama_gedung')),
				'latitude' => htmlspecialchars($this->input->post('latitude')),
				'longitude' => htmlspecialchars($this->input->post('longitude')),
				'active_gedung' => htmlspecialchars($this->input->post('active_gedung')),
				'img_gedung' => $upload1,
				'date_created' => date('Y-m-d H:i:s')
			];

			$log = [
				'log' => "Tambah Data Gedung = $nama_gedung",
				'email' => $this->session->userdata('email'),
				'date_created' => date('Y-m-d H:i:s')
			];

			$this->db->insert('swa_log', $log);
			$this->db->insert('swa_gedung', $data);

			$this->session->set_flashdata('sukses', 'Disimpan');
			redirect('v_aset');
		}
	}

	public function u_aset()
	{
		$id_gedung = $this->uri->segment(2);
		$data = array(
			'title' => 'Manajemen Aset',
			'active_menu_bar' => 'menu-open',
			'active_menu_br' => 'active',
			'active_menu_vaset' => 'active',
			'd' => $this->mm->detAset($id_gedung),
			'v' => $this->mm->getKantor()
		);

		$this->load->view('layout/header', $data);
		$this->load->view('manajemen/aset/u_aset', $data);
		$this->load->view('layout/footer');
	}

	public function u_aset_go()
	{

		$nama_gedung = htmlspecialchars($this->input->post('nama_gedung'));
		$upload_image = $_FILES['img_gedung']['name'];
		if ($upload_image) {
			$config['allowed_types'] = 'jpg|jpeg|png';
			$config['max_size']      = '10000';
			$config['upload_path'] = './src/img/';
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);

			if ($this->upload->do_upload('img_gedung')) {
				$upload1 = $this->upload->data('file_name');
				$this->db->set('img_gedung', $upload1);
			} else {
				echo $this->upload->display_errors();
			}
		}
		$id_gedung = htmlspecialchars($this->input->post('id_gedung'));
		$lokasi_id = htmlspecialchars($this->input->post('lokasi_id'));
		$nama_gedung = htmlspecialchars($this->input->post('nama_gedung'));
		$latitude = htmlspecialchars($this->input->post('latitude'));
		$longitude = htmlspecialchars($this->input->post('longitude'));
		$active_gedung = htmlspecialchars($this->input->post('active_gedung'));


		$this->db->set('lokasi_id', $lokasi_id);
		$this->db->set('nama_gedung', $nama_gedung);
		$this->db->set('latitude', $latitude);
		$this->db->set('longitude', $longitude);
		$this->db->set('active_gedung', $active_gedung);
		$this->db->where('id_gedung', $id_gedung);
		$this->db->update('swa_gedung');

		$log = [
			'log' => "Ubah Data Gedung = $id_gedung",
			'email' => $this->session->userdata('email'),
			'date_created' => date('Y-m-d H:i:s')
		];

		$this->db->insert('swa_log', $log);

		$this->session->set_flashdata('sukses', 'Disimpan');
		redirect('v_aset');
	}


	public function h_aset()
	{

		$id_gedung = $this->uri->segment(2);
		$active_gedung = 0;

		$this->db->set('active_gedung', $active_gedung);
		$this->db->where('id_gedung', $id_gedung);
		$this->db->update('swa_gedung');

		$log = [
			'log' => "Hapus Data Gedung = $id_gedung",
			'email' => $this->session->userdata('email'),
			'date_created' => date('Y-m-d H:i:s')
		];

		$this->db->insert('swa_log', $log);

		$this->session->set_flashdata('sukses', 'Disimpan');
		redirect('v_aset');
	}

	public function v_swa()
	{
		$data = array(
			'title' => 'Manajemen Swakelola',
			'active_menu_vswa' => 'active',
			'v' => $this->mm->getSwa()
		);
		$this->load->view('layout/header', $data);
		$this->load->view('manajemen/swakelola/v_swa', $data);
		$this->load->view('layout/footer');
	}


	public function c_swa()
	{
		$data = array(
			'title' => 'Manajemen Swakelola',
			'active_menu_vswa' => 'active',
			'v' => $this->mm->getSwa()
		);
		$this->form_validation->set_rules('jenis_swakelola', 'Nama Kantor', 'required');

		if ($this->form_validation->run() ==  false) {
			$this->load->view('layout/header', $data);
			$this->load->view('manajemen/swakelola/c_swa', $data);
			$this->load->view('layout/footer');
		} else {
			$ket = htmlspecialchars($this->input->post('jenis_swakelola'));
			$data = [
				'jenis_swakelola' => htmlspecialchars($this->input->post('jenis_swakelola')),
				'date_created' => date('Y-m-d H:i:s')
			];

			$log = [
				'log' => "Tambah Data Jenis Swakelola $ket",
				'email' => $this->session->userdata('email'),
				'date_created' => date('Y-m-d H:i:s')
			];

			$this->db->insert('swa_log', $log);
			$this->db->insert('swa_swakelola', $data);

			$this->session->set_flashdata('sukses', 'Disimpan');
			redirect('v_swa');
		}
	}


	public function u_swa()
	{
		$id_swakelola = $this->uri->segment(2);
		$data = array(
			'title' => 'Manajemen swakelola',
			'active_menu_vswa' => 'active',
			'd' => $this->mm->detSwa($id_swakelola)
		);

		$this->load->view('layout/header', $data);
		$this->load->view('manajemen/swakelola/u_swa', $data);
		$this->load->view('layout/footer');
	}


	public function u_swa_go()
	{

		$id_swakelola = htmlspecialchars($this->input->post('id_swakelola'));
		$jenis_swakelola = htmlspecialchars($this->input->post('jenis_swakelola'));


		$this->db->set('jenis_swakelola', $jenis_swakelola);
		$this->db->where('id_swakelola', $id_swakelola);
		$this->db->update('swa_swakelola');

		$log = [
			'log' => "Ubah Data Jenis Swakelola ID = $id_swakelola",
			'email' => $this->session->userdata('email'),
			'date_created' => date('Y-m-d H:i:s')
		];

		$this->db->insert('swa_log', $log);
		$this->session->set_flashdata('sukses', 'Diubah');
		redirect('v_swa');
	}

	public function h_swa()
	{
		$id_swakelola = $this->uri->segment(2);

		$this->db->where('id_swakelola', $id_swakelola);
		$this->db->delete('swa_swakelola');

		$log = [
			'log' => "Hapus Data jenis Swakelola ID = $id_swakelola",
			'email' => $this->session->userdata('email'),
			'date_created' => date('Y-m-d H:i:s')
		];

		$this->db->insert('swa_log', $log);

		$this->session->set_flashdata('sukses', 'Dihapus');
		redirect('v_swa');
	}


	public function v_ruangan()
	{
		$data = array(
			'title' => 'Manajemen Ruangan',
			'active_menu_bar' => 'menu-open',
			'active_menu_br' => 'active',
			'active_menu_vruangan' => 'active',
			'v' => $this->mm->getRuangan()
		);
		$this->load->view('layout/header', $data);
		$this->load->view('manajemen/ruangan/v_ruangan', $data);
		$this->load->view('layout/footer');
	}

	public function c_ruangan()
	{
		$data = array(
			'title' => 'Manajemen Ruangan',
			'active_menu_bar' => 'menu-open',
			'active_menu_br' => 'active',
			'active_menu_vruangan' => 'active',
			'gedung' => $this->mm->getAset()
		);
		$this->form_validation->set_rules('gedung_id', 'Nama Gedung', 'required');
		$this->form_validation->set_rules('lantai', 'Lantai', 'required');
		$this->form_validation->set_rules('nama_ruangan', 'Nama ruangan', 'required');

		if ($this->form_validation->run() ==  false) {
			$this->load->view('layout/header', $data);
			$this->load->view('manajemen/ruangan/c_ruangan', $data);
			$this->load->view('layout/footer');
		} else {
			$nama_ruangan = htmlspecialchars($this->input->post('nama_ruangan'));
			$data = [
				'gedung_id' => htmlspecialchars($this->input->post('gedung_id')),
				'lantai' => htmlspecialchars($this->input->post('lantai')),
				'nama_ruangan' => htmlspecialchars($this->input->post('nama_ruangan')),
				'date_created' => date('Y-m-d H:i:s')
			];

			$log = [
				'log' => "Tambah Data ruangan = $nama_ruangan",
				'email' => $this->session->userdata('email'),
				'date_created' => date('Y-m-d H:i:s')
			];

			$this->db->insert('swa_log', $log);
			$this->db->insert('swa_ruangan', $data);

			$this->session->set_flashdata('sukses', 'Disimpan');
			redirect('v_ruangan');
		}
	}

	public function u_ruangan()
	{
		$id_ruangan = $this->uri->segment(2);
		$data = array(
			'title' => 'Manajemen ruangan',
			'active_menu_bar' => 'menu-open',
			'active_menu_br' => 'active',
			'active_menu_vruangan' => 'active',
			'd' => $this->mm->detRuangan($id_ruangan),
			'v' => $this->mm->getAset()
		);

		$this->load->view('layout/header', $data);
		$this->load->view('manajemen/ruangan/u_ruangan', $data);
		$this->load->view('layout/footer');
	}

	public function u_ruangan_go()
	{

		$nama_ruangan = htmlspecialchars($this->input->post('nama_ruangan'));

		$id_ruangan = htmlspecialchars($this->input->post('id_ruangan'));
		$gedung_id = htmlspecialchars($this->input->post('gedung_id'));
		$lantai = htmlspecialchars($this->input->post('lantai'));
		$nama_ruangan = htmlspecialchars($this->input->post('nama_ruangan'));


		$this->db->set('gedung_id', $gedung_id);
		$this->db->set('nama_ruangan', $nama_ruangan);
		$this->db->set('lantai', $lantai);
		$this->db->where('id_ruangan', $id_ruangan);
		$this->db->update('swa_ruangan');

		$log = [
			'log' => "Ubah Data ruangan = $id_ruangan",
			'email' => $this->session->userdata('email'),
			'date_created' => date('Y-m-d H:i:s')
		];

		$this->db->insert('swa_log', $log);

		$this->session->set_flashdata('sukses', 'Disimpan');
		redirect('v_ruangan');
	}


	public function h_ruangan()
	{

		$id_ruangan = $this->uri->segment(2);

		$this->db->where('id_ruangan', $id_ruangan);
		$this->db->DELETE('swa_ruangan');

		$log = [
			'log' => "Hapus Data ruangan = $id_ruangan",
			'email' => $this->session->userdata('email'),
			'date_created' => date('Y-m-d H:i:s')
		];

		$this->db->insert('swa_log', $log);

		$this->session->set_flashdata('sukses', 'Disimpan');
		redirect('v_ruangan');
	}
}
