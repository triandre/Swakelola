<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Informasi extends CI_Controller
{
	public function __construct()
	{
        parent::__construct();
		is_logged_in();
		$this->load->model('Manajemen_mdl', 'mm');
		$this->load->model('Informasi_mdl', 'im');
		$this->load->library('uuid');
	}

	public function index()
	{
		$data = array(
			'title' => 'Informasi Tender',
			'active_menu_vinfo' => 'active',
			'v' => $this->im->getInformasi(),
            
		);
		$this->load->view('layout/header', $data);
		$this->load->view('informasi/index', $data);
		$this->load->view('layout/footer');
	}

	public function tambah()
	{
		$data = array(
			'title' => 'Tambah Informasi Tender',
			'active_menu_info' => 'active',
			'v' => $this->mm->getRuangan(),
			'swa' => $this->mm->getSwa()
		);
		$this->form_validation->set_rules('ruangan_id', 'Nama Gedung', 'required');
		$this->form_validation->set_rules('swakelola_id', 'Jenis Pengadaan', 'required');
		$this->form_validation->set_rules('judul_tender', 'Judul', 'required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
		$this->form_validation->set_rules('pagu_anggaran', 'Pagu ', 'required');

		if ($this->form_validation->run() ==  false) {
		$this->load->view('layout/header', $data);
		$this->load->view('informasi/c_info', $data);
		$this->load->view('layout/footer');
		} else {
			$id = $this->uuid->v4();
			$reff_tender = str_replace('-', '', $id);
			$email_created= $this->session->userdata('email');

			$upload_image = $_FILES['lampiran']['name'];
			if ($upload_image) {
				$config['allowed_types'] = 'pdf';
				$config['max_size']      = '10000';
				$config['upload_path'] = './src/archive/';
				$config['encrypt_name'] = TRUE;
	
				$this->load->library('upload', $config);
	
				if ($this->upload->do_upload('lampiran')) {
					$upload1 = $this->upload->data('file_name');
				} else {
					echo $this->upload->display_errors();
				}
			}
			$data=[
				'reff_tender' => $reff_tender,
				'ruangan_id' => htmlspecialchars($this->input->post('ruangan_id')),
				'swakelola_id' => htmlspecialchars($this->input->post('swakelola_id')),
				'judul_tender' => htmlspecialchars($this->input->post('judul_tender')),
				'keterangan' => htmlspecialchars($this->input->post('keterangan')),
				'pagu_anggaran' => preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('pagu_anggaran', true))),
				'lampiran' => $upload1,
				'email_created' => $email_created,
				'date_end_work' => htmlspecialchars($this->input->post('date_end_work')),
				'date_start_tender' => htmlspecialchars($this->input->post('date_start_tender')),
				'date_end_tender' => htmlspecialchars($this->input->post('date_end_tender')),
				'active_tender' =>1,
				'sts_tender' =>1,
				'date_created' => date('Y-m-d H:i:s')
			];

			$log = [
				'log' => "Tambah Data Informasi REFF ID = $reff_tender",
				'email' => $this->session->userdata('email'),
				'date_created' => date('Y-m-d H:i:s')
			];
	
			$this->db->insert('swa_log', $log);
			$this->db->insert('swa_tender', $data);
	
			$this->session->set_flashdata('sukses', 'Disimpan');
			redirect('informasi');
		}
	}

	public function detail()
	{
		$reff_tender = $this->uri->segment(2);
		$data = array(
			'title' => 'Data Informasi Tender',
			'active_menu_vinfo' => 'active',
            'd' => $this->im->detInformasi($reff_tender),
			'v' => $this->mm->getRuangan(),
			'swa' => $this->mm->getSwa()
		);

		$this->load->view('layout/header', $data);
		$this->load->view('informasi/d_info', $data);
		$this->load->view('layout/footer');
	}

		
	public function ubah()
	{
		$reff_tender = $this->uri->segment(2);
		$data = array(
			'title' => 'Ubag Informasi Tender',
			'active_menu_vinfo' => 'active',
            'd' => $this->im->detInformasi($reff_tender),
			'v' => $this->mm->getRuangan(),
			'swa' => $this->mm->getSwa()
		);

		$this->load->view('layout/header', $data);
		$this->load->view('informasi/u_info', $data);
		$this->load->view('layout/footer');
	}


	public function ubahGo()
	{

		    $reff_tender = htmlspecialchars($this->input->post('reff_tender'));
			$ruangan_id = htmlspecialchars($this->input->post('ruangan_id'));
			$swakelola_id = htmlspecialchars($this->input->post('swakelola_id'));
			$judul_tender = htmlspecialchars($this->input->post('judul_tender'));
			$keterangan = htmlspecialchars($this->input->post('keterangan'));
			$pagu_anggaran = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('pagu_anggaran', true)));
			$date_end_work = htmlspecialchars($this->input->post('date_end_work'));
			$date_start_tender = htmlspecialchars($this->input->post('date_start_tender'));
			$date_end_tender = htmlspecialchars($this->input->post('date_end_tender'));

			$upload_image = $_FILES['lampiran']['name'];
			if ($upload_image) {
				$config['allowed_types'] = 'pdf';
				$config['max_size']      = '10000';
				$config['upload_path'] = './src/archive/';
				$config['encrypt_name'] = TRUE;
	
				$this->load->library('upload', $config);
	
				if ($this->upload->do_upload('lampiran')) {
					$upload1 = $this->upload->data('file_name');
					$this->db->set('lampiran', $upload1);
				} else {
					echo $this->upload->display_errors();
				}
			}

			
			$this->db->set('ruangan_id', $ruangan_id);
			$this->db->set('swakelola_id', $swakelola_id);
			$this->db->set('judul_tender', $judul_tender);
			$this->db->set('keterangan', $keterangan);
			$this->db->set('pagu_anggaran', $pagu_anggaran);
			$this->db->set('date_end_work', $date_end_work);
			$this->db->set('date_start_tender', $date_start_tender);
			$this->db->set('date_end_tender', $date_end_tender);
			$this->db->where('reff_tender', $reff_tender);
			$this->db->update('swa_tender');
	 
			
			$log = [
				'log' => "Tambah Data Informasi REFF ID = $reff_tender",
				'email' => $this->session->userdata('email'),
				'date_created' => date('Y-m-d H:i:s')
			];
	
			$this->db->insert('swa_log', $log);
	
			$this->session->set_flashdata('sukses', 'Disimpan');
			redirect('informasi');
		
	}

	public function hapus()
	{
		
		$reff_tender = $this->uri->segment(2);
		$active_tender = 0;
				
        $this->db->set('active_tender', $active_tender);
        $this->db->where('reff_tender', $reff_tender);
        $this->db->update('swa_gedung');

			$log = [
				'log' => "Hapus Data Tender = $reff_tender",
				'email' => $this->session->userdata('email'),
				'date_created' => date('Y-m-d H:i:s')
			];
	
			$this->db->insert('swa_log', $log);
	
			$this->session->set_flashdata('sukses', 'Disimpan');
			redirect('informasi');
		
	}

}
