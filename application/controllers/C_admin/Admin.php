<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
	public function __construct()
	{
        parent::__construct();
		is_logged_in();
		$this->load->model('Manajemen_mdl', 'mm');
		$this->load->model('Informasi_mdl', 'im');
		$this->load->model('Penawaran_mdl', 'pm');
	}

	public function index()
	{
		$data = array(
			'title' => 'Manajemen Infomasi',
			'active_menu_db' => 'active',
			'v' => $this->im->getInformasiBeranda(),
			'win' => $this->pm->getMenang()
		);
		$this->load->view('layout/header', $data);
		$this->load->view('admin/index', $data);
		$this->load->view('layout/footer');
	}
}