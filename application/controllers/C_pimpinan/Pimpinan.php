<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Pimpinan extends CI_Controller
{
	public function __construct()
	{
        parent::__construct();
		is_logged_in();
	}



	public function index()
	{
		$data = array(
			'title' => 'e-Procruitment',
			'active_menu_db' => 'active',
		);
		$this->load->view('layout/header', $data);
		$this->load->view('pimpinan/index', $data);
		$this->load->view('layout/footer');
	}
}
