<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Lapker extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Manajemen_mdl', 'mm');
        $this->load->model('Informasi_mdl', 'im');
        $this->load->model('Penawaran_mdl', 'pm');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Laporan Kerja',
            'active_menu_lapker' => 'active',
            'v' => $this->pm->getLapker(),

        );
        $this->load->view('layout/header', $data);
        $this->load->view('lapker/index', $data);
        $this->load->view('layout/footer');
    }

    public function v_lapker()
    {
        $reff_tender = $this->uri->segment(2);
        $reff_penawaran = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Progres Pengerjaan',
            'active_menu_lapker' => 'active',
            'v' => $this->pm->listLapker($reff_tender),
            'd' => $this->pm->detailLapker($reff_tender),
            'reff' =>  $reff_tender,
            'penawaran' => $reff_penawaran,
        );

        $this->load->view('layout/header', $data);
        $this->load->view('lapker/v_lapker', $data);
        $this->load->view('layout/footer');
    }

    public function c_lapker()
    {
        $reff_tender = $this->uri->segment(2);
        $reff_penawaran = $this->uri->segment(3);
        $data = array(
            'title' => 'Tambah Progres Pengerjaan',
            'active_menu_lapker' => 'active',
            'd' => $this->pm->detailLapker($reff_tender),
            'reff' => $reff_tender,
            'penawaran' => $reff_penawaran,
        );
        $this->load->view('layout/header', $data);
        $this->load->view('lapker/c_lapker', $data);
        $this->load->view('layout/footer');
    }

    public function c_lapkerGo()
    {
        $id = $this->uuid->v4();
        $reff_lapker = str_replace('-', '', $id);
        $email_pt = $this->session->userdata('email');
        $reff = htmlspecialchars($this->input->post('reff_tender'));
        $penawaran = htmlspecialchars($this->input->post('reff_penawaran'));
        $upload_image = $_FILES['lampiran_lapker']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf|jpg|jpeg|png|doc|docx|xlx|xls';
            $config['max_size']      = '10000';
            $config['upload_path'] = './src/archive/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('lampiran_lapker')) {
                $upload1 = $this->upload->data('file_name');
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = [
            'reff_lapker' => $reff_lapker,
            'reff_tender' => htmlspecialchars($this->input->post('reff_tender')),
            'reff_penawaran' => htmlspecialchars($this->input->post('reff_penawaran')),
            'ket_progres' => htmlspecialchars($this->input->post('keterangan')),
            'email_pt' => $email_pt,
            'persen' => htmlspecialchars($this->input->post('persen')),
            'lampiran_lapker' => $upload1,
            'date_created' => date('Y-m-d H:i:s')
        ];

        $log = [
            'log' => "Tambah Laporan Kerja = $reff_lapker",
            'email' => $this->session->userdata('email'),
            'date_created' => date('Y-m-d H:i:s')
        ];

        $this->db->insert('swa_log', $log);
        $this->db->insert('swa_lapker', $data);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('v_lapker/' . $reff . '/' . $penawaran);
    }

    public function u_lapker()
    {
        $reff_lapker = $this->uri->segment(2);
        $reff_tender = $this->uri->segment(3);
        $reff_penawaran = $this->uri->segment(4);
        $data = array(
            'title' => 'Tambah Progres Pengerjaan',
            'active_menu_lapker' => 'active',
            'd' => $this->pm->ubahLapker($reff_lapker),
            'reff' => $reff_tender,
            'penawaran' => $reff_penawaran
        );
        $this->load->view('layout/header', $data);
        $this->load->view('lapker/u_lapker', $data);
        $this->load->view('layout/footer');
    }

    public function u_lapkerGo()
    {

        $reff_lapker = htmlspecialchars($this->input->post('reff_lapker'));
        $reff = htmlspecialchars($this->input->post('reff_tender'));
        $penawaran = htmlspecialchars($this->input->post('reff_penawaran'));
        $ket_progres = htmlspecialchars($this->input->post('keterangan'));
        $persen = htmlspecialchars($this->input->post('persen'));

        $upload_image = $_FILES['lampiran_lapker']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf|jpg|jpeg|png';
            $config['max_size']      = '10000';
            $config['upload_path'] = './src/archive/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('lampiran_lapker')) {
                $upload1 = $this->upload->data('file_name');
                $this->db->set('lampiran_lapker', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }


        $this->db->set('ket_progres', $ket_progres);
        $this->db->set('persen', $persen);
        $this->db->where('reff_lapker', $reff_lapker);
        $this->db->update('swa_lapker');


        $log = [
            'log' => "Ubah Data REFF ID = $reff_lapker",
            'email' => $this->session->userdata('email'),
            'date_created' => date('Y-m-d H:i:s')
        ];

        $this->db->insert('swa_log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('v_lapker/' . $reff . '/' . $penawaran);
    }

    public function indexAdmin()
    {
        $data = array(
            'title' => 'Laporan Kerja',
            'active_menu_lapker' => 'active',
            'v' => $this->pm->getLapkerAdmin(),

        );
        $this->load->view('layout/header', $data);
        $this->load->view('lapker/indexAdmin', $data);
        $this->load->view('layout/footer');
    }
}
